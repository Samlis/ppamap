from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


class Log(models.Model):
  action = models.CharField(max_length = 255)
  date = models.DateField(default = timezone.now)
  user = models.ForeignKey(User)
  
  def __unicode__(self):
    return self.action