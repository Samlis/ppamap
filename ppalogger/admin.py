from django.contrib import admin
from ppalogger.models import Log

class LogAdmin(admin.ModelAdmin):
  list_display = ('action', 'user', 'date')
  readonly_fields= ('action', 'user', 'date')
  
admin.site.register(Log, LogAdmin)
