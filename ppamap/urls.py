"""postapomap URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import  static
from django.conf import settings

urlpatterns = [

    url(r'^admin/', include(admin.site.urls)),
    url(r'^desc/(?P<id>[0-9]{1,128})$', "map.description.get_description"),
    url(r'^poicount$', 'map.poi.get_poi_count'),
    url(r'^poiiconlist$', 'map.poi.get_icon_list'),
    url(r'^poiicon$', 'map.poi.get_poi_icon'),
    url(r'poiremove', 'map.poi.remove_poi'),
    url(r'poisave', 'map.poi.save_poi'),
    url(r'^getfilterdate$', "map.views.get_filtered_objs"),
    url(r'^getfiltereddata$', "map.views.get_filtered_data"),
    url(r'^getjoinedfiltereddata$', "map.views.get_joined_filter_data"),
    
    url(r'^descsave$', 'map.description.save_desc'),
    url(r'^getdescstitles$', 'map.description.get_all_desc_ids'),
    url(r'getpoidialog', 'map.poi.get_poi_property_dialog'),
    url(r'getpolydialog', 'map.poly.get_poly_property_dialog'),
    url(r'^getoptionstitles$', 'map.description.get_all_option_ids'),
    url(r'^editor/getimglist$', 'map.description.get_picture_list'),
    url(r'^getUserNames', 'map.adminactions.get_users'),
    
    url(r'^$', 'map.views.create_view'),
    url(r'^get_data$', 'map.views.get_data'),
    url(r'^editor/(?P<desc_id>[0-9]{1,128})$', 'texteditor.views.get_editor_view'),
    url(r'^editor/new$', 'texteditor.views.get_editor_view'),
    url(r'^saverestrictionarea', 'map.adminactions.save_restriction_area'),
    url(r'^getuserarea', 'map.views.get_user_area'),
    
    url(r'^geteventlistview$', 'eventbook.views.create_eventbook_list_view'),
    url(r'^geteventlist/(?P<page>[0-9]{1,128})$', 'eventbook.views.get_eventbook_list'),
    url(r'^geteventdesc/(?P<id>[0-9]{1,128})$', 'eventbook.views.create_eventbook_desc'),
    
    url(r'removePoly', 'map.poly.remove_poly'),
    url(r'savepolygon', 'map.poly.save_polygon'),
    
    url(r'^login$', 'map.authentication.user_login'),
    url(r'^logout$','map.authentication.user_logout'),
    url(r'^tutorial$', 'map.views.show_tutorial'),
    
    url(r'^encyclopedia/$', 'encyclopedia.views.get_encyclopedia_view'),
    url(r'^encyclopedia/(?P<desc_id>[0-9]{1,128})$', 'encyclopedia.views.get_encyclopedia_view'),
]
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)