def check_permissions(request, *args):
  all_perms = list(request.user.get_all_permissions())
  authorize = False
  for arg in args:
    if arg not in all_perms:
      authorize = True
  return authorize