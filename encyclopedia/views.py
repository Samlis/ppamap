import json

from django.shortcuts import render
from map.models import Description, POI, Polygons, Faction, EventType, ObjectType
from django.http.response import JsonResponse
from django.utils import timezone

def get_encyclopedia_view(request, desc_id = -1):
  if request.method == "POST":
    description = Description.objects.filter(id = desc_id).values('id', 'title', 'author', 'creation_date', 'modification_date', 'description')[0]
    description = dict(description)
    description['creation_date'] = timezone.localtime(description['creation_date']).strftime('%H:%M:%S %d.%m.%Y')
    description['modification_date'] = timezone.localtime(description['modification_date']).strftime('%H:%M:%S %d.%m.%Y') 
    return JsonResponse(description)
  elif request.method == "GET":
    desc_filters = _get_descriptions_with_filter_data()    
    description = None
    if desc_id > -1:
      try:
        description = Description.objects.filter(id = desc_id).values('id', 'title', 'author','creation_date', 'modification_date', 'description')[0]
      except Description.DoesNotExist():
        pass
    descriptions = Description.objects.values('id', 'title').order_by('title')
    years = set()
    for desc in descriptions:
      if desc_filters.get(str(desc['id'])):
        desc.update(desc_filters[str(desc['id'])])
        years.add(desc_filters[str(desc['id'])]['date'])
    factions = Faction.objects.all().values('id', 'name')
    events = EventType.objects.all().values('id', 'name')
    objects = ObjectType.objects.all().values('id', 'name')
    
    return render(request, 
                  'encyclopedia-view.html',
                  {'descriptions': descriptions,
                   'opened': description,
                   'factions': factions,
                   'events': events,
                   'objects': objects,
                   'years': years})
  

def _get_descriptions_with_filter_data():
  pois = list(POI.objects.all().values('action_date', 'faction_id', 'event_type_id', 'object_type_id', 'poi_desc_id', 'extra_desc')) + list(Polygons.objects.all().values('action_date', 'faction_id', 'event_type_id', 'object_type_id', 'poly_desc_id', 'extra_desc'))
  desc_filters = {}
  for poi in pois:
    try:
      tmp = poi.get('extra_desc', '').split(';')
    except Exception as e:
      print e
      continue
    for desc_info in tmp:
      desc_tmp = desc_info.split(',')
      if not desc_filters.has_key(desc_tmp[0]):
        desc_filters[desc_tmp[0]] = {"faction": [],
                                     "event": [],
                                     "obj": [],
                                     "date": None }
      if poi.get('faction_id') and poi['faction_id'] not in desc_filters[desc_tmp[0]]['faction']:
        desc_filters[desc_tmp[0]]['faction'].append(poi['faction_id'])
        
      if poi.get('event_type_id') and poi['event_type_id'] not in desc_filters[desc_tmp[0]]['event']:
        desc_filters[desc_tmp[0]]['event'].append(poi['event_type_id'])
        
      if poi.get('object_type_id') and poi['object_type_id'] not in desc_filters[desc_tmp[0]]['obj']:
        desc_filters[desc_tmp[0]]['obj'].append(poi['object_type_id'])
      
      if poi.get('action_date'):
        date = poi['action_date'].strftime("%Y")
        if date > desc_filters[desc_tmp[0]]['date']:
          desc_filters[desc_tmp[0]]['date'] = date
        
  return desc_filters