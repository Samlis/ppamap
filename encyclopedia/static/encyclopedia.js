function init() {
  $('button.filters').on('click', onFilterBoxClick);
  $('li[did]').on('click', onDescriptionClick);
  $('button.confirm-filters').on('click', runFilter);
}

function onFilterBoxClick() {
  var $filters = $('div.filter-box');
  if($filters.is(':visible')) {
    $filters.hide();
  } else {
    $filters.css('left', $('div.left-view').width())
    $filters.show()
  }
}

function runFilter() {
  var $filters = $('div.filter-box');
  var $selects = $filters.find('select');
  var notSelected = true;
  var $list = $('.list-view li');
  $list.hide();
  
  
  for(var i = 0; i < $selects.length; i++) {
    var id = $selects.eq(i).attr('id');
    var value = $selects.eq(i).val();
    
    if(value == -1) {
      continue
    }
    
    var filter = ""
    if(id == "date")
      filter = '[date="'+value+'"]';
    else
      filter = '['+id+'*="'+value+',"]';
    
    $list = $list.filter(filter);
  }
  
  $list.show();
}

function onDescriptionClick() {
  var did = $(this).attr('did');
  if(did == undefined)
    return
    
  $.ajax({
    url: "/encyclopedia/" + did,
    type: "POST",
    data: {"":""}
  })
  .done(onDescriptionSuccess)
  .fail(function() {
    console.log('loled');
  });
}

function onDescriptionSuccess(resp) {
  var $right = $('.right-view');
  var $info = $right.find('.info td.txt');
  $right.find('.title span').text(resp.title);
  $info.eq(0).text(resp.creation_date);
  $info.eq(1).text(resp.modification_date);
  $info.eq(2).text(resp.author);
  $right.find('.content').html(resp.description);
  history.pushState(null, null, "/encyclopedia/" + resp.id);
}

$(window).load(init);