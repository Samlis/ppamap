from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from map.models import POI, Polygons

class PAEvent(models.Model):
  user = models.ForeignKey(User)
  title = models.CharField(max_length = 255)
  creation_date = models.DateTimeField(default = timezone.now)
  modification_date = models.DateTimeField(default = timezone.now)
  description = models.TextField(blank = True, null = True )
  price = models.DecimalField(default = 0.0, max_digits = 8, decimal_places = 2)
  start_date = models.DateTimeField()
  end_date = models.DateTimeField()
  place = models.CharField(max_length = 255)
  poi = models.ForeignKey(POI, blank = True, null = True)
  polygon = models.ForeignKey(Polygons, blank = True, null = True)
  link = models.URLField(blank = True, null = True)