from django.shortcuts import render
from models import PAEvent
from django.http.response import HttpResponse
from django.core.paginator import Paginator

def create_eventbook_list_view(request):
  event_list = list(PAEvent.objects.all())
  event_list = sorted(event_list, key = lambda x: x.start_date, reverse = True)
  pages = Paginator(event_list, 5)
  list_part = render(request, 'eventbook-list.html', {"paevents": pages.page(1)})
  return render(request, "eventbook-side-panel.html", {"paevent": list_part.content, "pages": pages._get_num_pages(), "page_no": 1})

def get_eventbook_list(request, page = 1):
  event_list = list(PAEvent.objects.all())
  event_list = sorted(event_list, key = lambda x: x.start_date, reverse = True)
  pages = Paginator(event_list, 5)
  return render(request, "eventbook-list.html", {"paevents": pages.page(page)})

def create_eventbook_desc(request, id):
  event = PAEvent.objects.get(id = id)
  return render(request, "eventbook-full-description.html", {"event" : event})