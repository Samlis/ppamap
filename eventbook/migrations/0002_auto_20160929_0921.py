# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('eventbook', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paevent',
            name='creation_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='paevent',
            name='modification_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
