# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('map', '0017_auto_20160816_1823'),
    ]

    operations = [
        migrations.CreateModel(
            name='PAEvent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('creation_date', models.DateTimeField(default=datetime.datetime(2016, 8, 16, 16, 23, 46, 856000, tzinfo=utc))),
                ('modification_date', models.DateTimeField(default=datetime.datetime(2016, 8, 16, 16, 23, 46, 856000, tzinfo=utc))),
                ('description', models.TextField(null=True, blank=True)),
                ('price', models.DecimalField(default=0.0, max_digits=8, decimal_places=2)),
                ('start_date', models.DateTimeField()),
                ('end_date', models.DateTimeField()),
                ('place', models.CharField(max_length=255)),
                ('link', models.URLField(null=True, blank=True)),
                ('poi', models.ForeignKey(blank=True, to='map.POI', null=True)),
                ('polygon', models.ForeignKey(blank=True, to='map.Polygons', null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
