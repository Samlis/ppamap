from django.contrib import admin
from models import PAEvent

class PAEventAdmin(admin.ModelAdmin):
  list_display = ('title','creation_date', 'start_date', 'user')
  
admin.site.register(PAEvent, PAEventAdmin)
