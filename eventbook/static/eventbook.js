$(function() {
  
  $(window).on('load', init);
  var $showButton;
  var $listDialog;
  var $page;
  var $lastPage;
  var $prevButton;
  var $nextButton;
  var downloading = false;
  
  function init(){
    $showButton = $('button#showEventBook');
    $showButton.on('click', toggleList);
    addListDialogToDom();
  }
  
  function addListDialogToDom() {
    $.ajax({
      url: "geteventlistview",
      type: "GET",
      })
    .done(function(resp) {
      $('#dynamicContent').append(resp);
      $listDialog = $('div#eventBookSidePanel');
      $listDialog.hide();
      $listDialog.find('button.hide-dialog').on('click', hideList);
      $prevButton = $listDialog.find('button.prev-event-list');
      $prevButton.on('click', getPrevPage);
      $nextButton = $listDialog.find('button.next-event-list');
      $nextButton.on('click', getNextPage);
      $page = $listDialog.find('span.page_no');
      $lastPage = $listDialog.find('span.max_pages');
      $('div.event-book-row').on('click', onClickEventDetails);
    });
  }
  
  function getPrevPage() {
    var $that = $(this);
    var actualPage = parseInt($page.html());
    if(actualPage - 1 <= 0)
      return
    getPageData(actualPage - 1);
  }
  
  function getNextPage() {
    var $that = $(this);
    var actualPage = parseInt($page.html());
    var lastPage = parseInt($lastPage.html());
    if(actualPage + 1 > lastPage)
      return
    getPageData(actualPage + 1);
  }
  
  function getPageData(page) {
    if(downloading)
      return
      
    downloading = true;  
    $listDialog.find('.event-book-holder').html('');
    addAjaxLoad($listDialog.find('.event-book-holder'));
    
    $.ajax({
      url: "geteventlist/" + page,
      type: "GET",
    })
    .done(function(resp) {
      $listDialog.find('.event-book-holder').html(resp);
      $page.html(page);
      
      if(page > 1)
        $prevButton.removeAttr('disabled');
      else if (page == 1)
        $prevButton.attr('disabled', true);
      
      if(page == parseInt($lastPage.html()) && !$nextButton.is('disabled'))
        $nextButton.attr('disabled', true);
      else if (page < parseInt($lastPage.html()))
        $nextButton.removeAttr('disabled');
      
      $('div.event-book-row').on('click', onClickEventDetails);
    })
    .always(function() {
      downloading = false;
    });
  }
  
  function onClickEventDetails(e) {
    var $that = $(this);
    $.ajax({
      url:"geteventdesc/" + $that.attr('paevent-id'),
      type: "GET",
    })
    .done(function(resp){
      $(resp).dialog({
        title: "Event",
        maxHeight: 400,
        width: 600,
        modal: true,
        closeOnEscape: true,
        close: closeDialog,
      });
      $('button.event-place-clicker').on('click', moveToPosition);
    });
  }
  
  function moveToPosition() {
    var $that = $(this);
    var cords = $that.attr('cords').split('/');
    var $dialog = $('#eventBookFullDesc');
    $dialog.dialog('close');
    $dialog.remove();
    hideList();
    map.setCenter({lat: parseFloat(cords[0]), lng: parseFloat(cords[1])});
    map.setZoom(12);
  }
  
  function closeDialog(){
    $(this).dialog('close');
    $(this).remove();
  }
  
  function showList() {
    $listDialog.show('slide', {direction:'up'}, 500);
  }
  
  function hideList() {
    $listDialog.hide('slide', {direction:'up'}, 500);
  }
  
  function toggleList() {
    $listDialog.toggle('slide', {direction:'up'}, 500);
  }
  
});