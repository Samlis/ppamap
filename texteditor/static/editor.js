$('button.text-tag-button').on('click', tagSelect);
$('button.text-style-button').on('click', styleSelect);
$('button.preview').on('click', changeEditorMode);
$('button#save').on('click', saveDescription);
$('button.set-color-button').on('click', addColorStyle);
$('button#link').on('click', showUrlCreator);
$('button#img').on('click', showImageAddDialog);

$('button.html-editor').on('click', toggleEditorMode)
$('input.colorpicker').on('change', function(){
  var color = $(this).val();
  $('button.set-color-button').each(function() {
    $(this).css($(this).attr('tag'), color);
  });
});

var plainHTMLMode = false;
var customTagReplacement = {
    "pleft": "<p align=left> ",
    "pright": "<p align=right> ",
    "pcenter": "<p align=center> ",
    "pjustify": "<p align=justify> "
};

function addColorStyle(evt) {
  addTag("["+$(this).attr('tag')+":"+$('input.colorpicker').val()+"] ", "[/]");
}

function styleSelect(evt) {
  var tag = $(this).attr('tag');
  addTag("["+tag+"] ", "[/]");
}

function tagSelect(evt) {
  var tag = $(this).attr('tag');
  if(typeof customTagReplacement[tag] !== 'undefined' )
    addTag(customTagReplacement[tag], "</p>");
  else
    addTag("<"+tag+"> ", "</"+tag+">");
}

function addTag(startTag, endTag) {
  var $textarea = $('#editor');
  var value = $textarea.val();
  var start = value.substring(0,$textarea[0].selectionStart);
  var mid = value.substring($textarea[0].selectionStart, $textarea[0].selectionEnd);
  var end = value.substring($textarea[0].selectionEnd, value.lenght);
  $textarea.val( start + startTag + mid + endTag + end );
}

function changeEditorMode(evt) {
  var mode = $(this).attr('mode');
  if(mode == "editor")
    showPreview(this);
  else if(mode == "preview")
    showEditor(this);
}

function showEditor(that) {
  var $that = $(that);
  $('div.preview').hide();
  $('#editor').show();
  $that.attr('mode','editor');
  $that.html('Preview')
}

function showPreview(that) {
  var $preview = $('div.preview');


  var $editor = $('#editor');
  var value = $editor.val();
  var $that = $(that);
  $editor.hide();
  if(plainHTMLMode)
    $preview.html(value);
  else
    $preview.html(parseTextToHtml(value));
  $preview.show();
  $that.attr('mode','preview');
  $that.html('Editor')
}

String.prototype.replaceAt=function(index, character) {
    return this.substring(0, index) + character + this.substring(index+1, this.length);
}

function saveDescription(){
  var $editor = $('#editor');
  var value = "";
  if(plainHTMLMode)
    value = $editor.val();
  else
    value = parseTextToHtml($editor.val());
  var data = {
    "id": $editor.attr('descid'),
    "desc": value,
    "title": $('input#title').val(),
  };
  
  $.ajax({
    url: "/descsave",
    type: "POST",
    data: data,
    success: function(resp){
      if(resp != "error") {
        $editor.attr('descid', resp);
        alert("saved with id: " + resp);
      }
    },
    failed: function(){
      alert("failed");
    }
  });
}

function parseTextToHtml(data) {
  var splitted = data.split('[/]');
  var newText = "";
  for(var i = 0; i < splitted.length; i++) {
    if(splitted[i].length == 0)
      continue
    var e = parseToStyle(splitted[i]);
    newText += e; 
  }
  return newText
}

function parseToStyle(line) {
  var styles = [];
  var newLine = line;
  while(newLine.search(/\[*\]/g) > -1) {
    var style = newLine.substring(newLine.indexOf('['), newLine.indexOf(']')+1);
    newLine = newLine.replace(style ,'');
    style = style.replace('[',' ');
    style = style.replace(']',';');
    styles.push(style);
  }
  var firstTag = line.search(/\<[^\/]*\>/);
  var endFirstTag = line.indexOf('>', firstTag);
  if(firstTag > -1 && (line.indexOf("[") - endFirstTag < 2)) {
    var styleTag = line.indexOf("style=", firstTag);
    if(styleTag > -1)
      newLine = newLine.replaceAt(styleTag+6, '"' + styles.join(' '));
    else
      newLine = newLine.replaceAt(line.indexOf(">", firstTag), ' style="' + styles.join(' ') + '">');
  }
  else if(styles.length){
    newLine = newLine.replaceAt(line.indexOf("["), '<p style="' + styles.join(' ') + '">');
    newLine += "</p>";
  } else
    return line
  return newLine
}

function showUrlCreator() {
  var $select;
  $('<div id="urladder"></div>').html(createUrlDialogView()).dialog({
    modal: true,
    width: 400,
    close: closeDialogAction,
    buttons: [
              {
                text: "Cancel",
                click: closeDialogAction
              },
              {
                text: "Add",
                click: addUrlToText
              }
              ]
  });
  $('div#urladder').dialog('open');
  
  $select = $('select#inner-urls');
  $select.prop('disabled',true);
  
  addSelectOptions("/getdescstitles", $select);
  
  $('input#inner-checkbox').on('change', function() {
    var $that = $(this);
    if($that.is(':checked')){
      $('input#url').prop('disabled', true);
      $('select#inner-urls').prop('disabled',false);
    } else {
      $('input#url').prop('disabled', false);
      $('select#inner-urls').prop('disabled',true);
    }
  });
}

function addSelectOptions(url, $obj, args) {
  $.ajax({
    url:url,
    data: args ? args : {'':''},
    type:'POST',
    success: function(resp) {
      for(var i=0; i<resp['descs'].length; i++)
        $obj.append('<option value="'+resp["descs"][i]['id']+'">'+resp["descs"][i]['title']+'</option>');
    },
    failed: function() {
      alert('failed');
    }
  });
}

function createUrlDialogView() {
  var html = "<table class='dialog-table'>";
  html += "<tr><td>Url</td><td><input type='url' id='url'></td></tr>";
  html += "<tr><td><input type='checkbox' id='inner-checkbox'></td><td>Inner Content</td><td><select id='inner-urls'></select></td></tr></table>";
  return html;
}

function showImageAddDialog() {
  $("<div id='text-editor-image-dialog'></div>").html(createImageDialogView()).dialog({
    modal:true,
    width:300,
    title: "Add Image Dialog...",
    height:300,
    close: closeDialogAction,
    buttons: [
              {
                text: "Add image",
                click: addImage
              }
              ]
  });
  $('div#text-editor-image-dialog').dialog('open');
  $('button#choose-image').on('click', function() {
    var imgCh = new imgChooser('getimglist', $(this), 'image');
    imgCh.showDialog();
  });
  $('button#newImg').on('click', function() {
    window.open("/admin/map/image/add/", "Add new Image", "height=330,width=800");
  });
}

function addImage() {
  var $button = $('button#choose-image');
  var position = $('input[name="position"]:checked').val();
  var imgId = $button.attr('pic-id');
  var thumbUrl = $button.find('span').css('background-image').replace('url(', '').replace(')','');
  var tag = "<img src='"+thumbUrl+"' pic-id='"+imgId+"' class='img-thumb' align='"+position+"'>";
  addTag(tag, "");
  $(this).dialog('close');
  $(this).remove();
}

function createImageDialogView() {
  var html = "<table class='dialog-table'>";
  html += "<tr><td>"+ createImagePositionRadio("left") +"</td><td>"+ createImagePositionRadio("user") +"</td><td>"+ createImagePositionRadio("right") +"</td></tr>";
  html += "<tr><td colspan=2><button id='choose-image'>Choose Image</button></td><td><button id='newImg'>Add New Image</button></td></tr>";
  html += "</table>"
  return html
}

function createImagePositionRadio(position) {
  var html = "<input type='radio' name='position' value="+position+">";
  html += "<div style='border: 1px solid white; width:64px; height:64px;'>";
  if(position == "user") {
    html += "<div style='font-size:3pt; color:white; padding-left: 1px;'>"+randomTextGenerator(4)+"</div>";
    html += "<div style='background:white; width:16px; height:16px; position:relative; left:20px; top: -20px;'>";
    html += "</div></div>";
  }
  else {  
    html += "<div style='background:white; width:16px; height:16px; float:"+position+";'>";
    html += "</div><p style='font-size:3pt; color:white; padding-left: 1px;'>"+randomTextGenerator(3)+"</p></div>";
  }
  return html
}

function randomTextGenerator(multiply) {
  var txt = "";
  for(var i = 0; i < multiply; i++)
    txt += "random text random text random text random text ";
  return txt
}

function addUrlToText() {
  var $select = $('select#inner-urls');
  var $input = $('input#url');
  if(!$select.prop('disabled'))
    addTag('<a href="#" class="aInnerClick" desc-id="'+$select.val()+'">', '</a>');
  else if(!$input.prop('disabled'))
    addTag('<a href="'+$input.val()+'">', '</a>');
  $(this).dialog('close');
  $(this).remove();
}

function closeDialogAction() {
  $(this).dialog('close');
  $(this).remove();
}

function toggleEditorMode() {
  var $that = $(this);
  plainHTMLMode = !plainHTMLMode;
  if(plainHTMLMode) {
    $that.text('PRIM Editor');
    $('div.text-style button').attr("disabled", true);
    $('div.objects button').attr("disabled", true);
    $('div.text-position button').attr("disabled", true);

  }
  else {
    $that.text('HTML Editor');
    $('div.text-style button').removeAttr("disabled");
    $('div.objects button').removeAttr("disabled");
    $('div.text-position button').removeAttr("disabled");
  }
}