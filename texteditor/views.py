from django.shortcuts import render
from django.views.decorators.csrf import ensure_csrf_cookie
from map.models import Description

def get_editor_view(request, desc_id = None):
  all_perms = list(request.user.get_all_permissions())
  if("map.add_description" not in all_perms and "map.change_description" not in all_perms):
    return render(request, "500.html")
  if desc_id:
    desc = Description.objects.get(id = desc_id)
    return render(request, "editor.html", {'desc': desc})
  return render(request, "editor.html", {'desc': None})