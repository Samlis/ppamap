from map.views import get_filtered_data
from django.test import TestCase, Client
from map.models import EventType, Faction, POI, ObjectType, Polygons


class TestCaseMapViews(TestCase):

  @classmethod
  def setUpClass(cls):
    cls._add_map_objects()

  @classmethod
  def _add_map_objects(cls):
    ev1 = EventType.objects.create('event')
    ev2 = EventType.objects.create('another event')
    f1 = Faction.objects.create('faction')
    f2 = Faction.objects.create('another faction')
    ot1 = ObjectType.objects.create('object')
    ot2 = ObjectType.objects.create('another object')
    POI.objects.create(lat=1, lng=2, title='poi1', faction=f1, object_type=ot2)
    POI.objects.create(lat=3, lng=3, title='poi2', faction=f2, event_type=ev1)
    POI.objects.create(lat=5, lng=5, title='poi3', faction=f1)
    Polygons.objects.create(paths="[[12,2], [12,5], [14, 5]]", stroke_color="#ffeeff", fill_color="#ffeeff",
                            fill_opacity=1, stroke_opacity=1, faction=f1)
    Polygons.objects.create(paths="[[12,2], [12,5], [14, 5]]", stroke_color="#ffeeff", fill_color="#ffeeff",
                            fill_opacity=1, stroke_opacity=1, faction=f2)
    Polygons.objects.create(paths="[[12,2], [12,5], [14, 5]]", stroke_color="#ffeeff", fill_color="#ffeeff",
                            fill_opacity=1, stroke_opacity=1, faction=f1, event=ev1)

  def test_get_filtered_data_should_return_all_ev1_pois_and_polygons(self):
    client = Client()
    result = client.post('/getfiltereddata', {'events': ['event']})
    self.assertEqual()

