# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0016_auto_20160726_1957'),
    ]

    operations = [
        migrations.AlterField(
            model_name='description',
            name='creation_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 8, 16, 16, 23, 46, 843000, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='description',
            name='modification_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 8, 16, 16, 23, 46, 843000, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='poi',
            name='creation_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 8, 16, 16, 23, 46, 849000, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='poi',
            name='modification_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 8, 16, 16, 23, 46, 849000, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='polygons',
            name='creation_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 8, 16, 16, 23, 46, 845000, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='polygons',
            name='modification_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 8, 16, 16, 23, 46, 845000, tzinfo=utc)),
        ),
    ]
