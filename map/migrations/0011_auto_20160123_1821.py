# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0010_objecttype'),
    ]

    operations = [
        migrations.AddField(
            model_name='poi',
            name='object_type',
            field=models.ForeignKey(blank=True, to='map.ObjectType', null=True),
        ),
        migrations.AddField(
            model_name='polygons',
            name='object_type',
            field=models.ForeignKey(blank=True, to='map.ObjectType', null=True),
        ),
    ]
