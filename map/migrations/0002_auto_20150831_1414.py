# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='poi',
            name='icon',
            field=models.ImageField(default=b'null', upload_to=b'icons', blank=True),
        ),
    ]
