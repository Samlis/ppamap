# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0007_auto_20150929_2011'),
    ]

    operations = [
        migrations.AddField(
            model_name='poi',
            name='action_date',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='polygons',
            name='action_date',
            field=models.DateField(null=True, blank=True),
        ),
    ]
