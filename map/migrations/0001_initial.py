# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=128)),
                ('img', models.ImageField(upload_to=b'image')),
                ('thumbnail', models.ImageField(null=True, upload_to=b'image/thumb/', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='POI',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default=b'title', max_length=255)),
                ('icon', models.ImageField(null=True, upload_to=b'icons', blank=True)),
                ('lat', models.DecimalField(max_digits=12, decimal_places=9)),
                ('lng', models.DecimalField(max_digits=12, decimal_places=9)),
            ],
        ),
        migrations.CreateModel(
            name='POIDescription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default=b'title', max_length=255)),
                ('author', models.CharField(max_length=100, null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='PolyDescription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default=b'title', max_length=255)),
                ('author', models.CharField(max_length=100, null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Polygons',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('stroke_color', models.CharField(help_text=b'only # format: #rrggbb', max_length=7)),
                ('stroke_opacity', models.DecimalField(max_digits=3, decimal_places=2)),
                ('stroke_weight', models.IntegerField(default=1)),
                ('fill_color', models.CharField(help_text=b'only # format: #rrggbb', max_length=7)),
                ('fill_opacity', models.DecimalField(max_digits=3, decimal_places=2)),
                ('paths', models.TextField()),
                ('poly_desc', models.ForeignKey(blank=True, to='map.PolyDescription', null=True)),
            ],
        ),
        migrations.AddField(
            model_name='poi',
            name='poi_desc',
            field=models.ForeignKey(blank=True, to='map.POIDescription', null=True),
        ),
    ]
