# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

class Migration(migrations.Migration):

    dependencies = [
        ('map', '0004_image_typ'),
    ]

    operations = [
      migrations.RemoveField(
            model_name='poi',
            name='icon',
      ),
      migrations.AddField(
            model_name='poi',
            name='icon',
        field=models.ForeignKey(blank=True, to='map.Image', null=True),
      ),
    ]
