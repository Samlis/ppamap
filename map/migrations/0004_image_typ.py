# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0003_auto_20150906_1813'),
    ]

    operations = [
        migrations.AddField(
            model_name='image',
            name='typ',
            field=models.CharField(default=b'P', max_length=1, choices=[(b'I', b'POI Icon'), (b'P', b'Picture')]),
        ),
    ]
