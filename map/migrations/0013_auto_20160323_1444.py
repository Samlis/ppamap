# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-23 13:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0012_userarea'),
    ]

    operations = [
        migrations.AddField(
            model_name='poi',
            name='extra_desc',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='polygons',
            name='extra_desc',
            field=models.TextField(blank=True, null=True),
        ),
    ]
