# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0005_auto_20150929_1953'),
    ]

    operations = [
        migrations.RenameField(
            model_name='poi',
            old_name='icon',
            new_name='icon_id',
        ),
    ]
