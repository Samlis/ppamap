# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0002_auto_20150831_1414'),
    ]

    operations = [
        migrations.CreateModel(
            name='Description',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default=b'title', max_length=255)),
                ('type', models.CharField(default=b'poi', max_length=64)),
                ('author', models.CharField(max_length=100, null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
            ],
        ),
        migrations.AlterField(
            model_name='poi',
            name='poi_desc',
            field=models.ForeignKey(blank=True, to='map.Description', null=True),
        ),
        migrations.AlterField(
            model_name='polygons',
            name='poly_desc',
            field=models.ForeignKey(blank=True, to='map.Description', null=True),
        ),
        migrations.DeleteModel(
            name='POIDescription',
        ),
        migrations.DeleteModel(
            name='PolyDescription',
        ),
    ]
