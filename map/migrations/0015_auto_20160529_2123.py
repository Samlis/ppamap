# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0014_auto_20160330_1950'),
    ]

    operations = [
        migrations.AddField(
            model_name='description',
            name='creation_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 29, 19, 23, 33, 640000, tzinfo=utc)),
        ),
        migrations.AddField(
            model_name='poi',
            name='creation_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 29, 19, 23, 33, 640000, tzinfo=utc)),
        ),
        migrations.AddField(
            model_name='polygons',
            name='creation_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 29, 19, 23, 33, 640000, tzinfo=utc)),
        ),
    ]
