# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0006_auto_20150929_1959'),
    ]

    operations = [
        migrations.RenameField(
            model_name='poi',
            old_name='icon_id',
            new_name='icon',
        ),
    ]
