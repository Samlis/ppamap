# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0015_auto_20160529_2123'),
    ]

    operations = [
        migrations.AddField(
            model_name='description',
            name='modification_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 7, 26, 17, 57, 57, 299000, tzinfo=utc)),
        ),
        migrations.AddField(
            model_name='poi',
            name='modification_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 7, 26, 17, 57, 57, 304000, tzinfo=utc)),
        ),
        migrations.AddField(
            model_name='polygons',
            name='modification_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 7, 26, 17, 57, 57, 301000, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='description',
            name='creation_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 7, 26, 17, 57, 57, 299000, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='poi',
            name='creation_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 7, 26, 17, 57, 57, 304000, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='polygons',
            name='creation_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 7, 26, 17, 57, 57, 301000, tzinfo=utc)),
        ),
    ]
