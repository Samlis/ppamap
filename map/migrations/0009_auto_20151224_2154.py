# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0008_auto_20151012_2028'),
    ]

    operations = [
        migrations.CreateModel(
            name='EventType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Faction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255)),
                ('icon', models.ForeignKey(blank=True, to='map.Image', null=True)),
            ],
        ),
        migrations.AddField(
            model_name='poi',
            name='event_type',
            field=models.ForeignKey(blank=True, to='map.EventType', null=True),
        ),
        migrations.AddField(
            model_name='poi',
            name='faction',
            field=models.ForeignKey(blank=True, to='map.Faction', null=True),
        ),
        migrations.AddField(
            model_name='polygons',
            name='event_type',
            field=models.ForeignKey(blank=True, to='map.EventType', null=True),
        ),
        migrations.AddField(
            model_name='polygons',
            name='faction',
            field=models.ForeignKey(blank=True, to='map.Faction', null=True),
        ),
    ]
