import json
from django.views.decorators.csrf import ensure_csrf_cookie
from ppamap.security_utils import check_permissions
from django.http.response import HttpResponse
from django.contrib.auth.models import User
from map.models import UserArea

def get_users(request):
  if check_permissions(request, "map.add_userarea", "map.change_userarea"):
    HttpResponse("No access to this data");
  usr_out = []
  for usr in list(User.objects.all()):
    usr_out.append([usr.id, usr.username])
  return HttpResponse(json.dumps({"usr" : usr_out}), content_type="application/json")

@ensure_csrf_cookie
def save_restriction_area(request):
  if check_permissions(request, "map.add_userarea", "map.change_userarea"):
    HttpResponse("No access to save areas")
  try:
    data = request.POST
    usr = User.objects.get(id = data['user'])
    area = UserArea.objects.create(
                                   user = usr,
                                   area = data['poly']
                                   )
    return HttpResponse("1")
  except Exception as e:
    return HttpResponse(json.dumps({"errors": e.message}), content_type="application/json")
    