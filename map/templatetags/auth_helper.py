from django import template
from ppamap.security_utils import check_permissions

register = template.Library()

@register.filter
def check_user_permissions(request, action):
  return not check_permissions(request, action)