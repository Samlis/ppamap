import json
from ppamap.security_utils import check_permissions
from models import POI, Image, EventType, Faction, UserArea, Description
from django.http.response import HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie
from decimal import Decimal
from map.description import get_desc_obj
from map.models import ObjectType, Description
from ppalogger.logger import add_log
from map.collisions import in_sector
from django.shortcuts import render


def _get_poi_data(pois):
  poi_out = []
  for poi in list(pois):
      poi_out.append({
                      "id": poi.id,
                      "lat": str(poi.lat),
                      "lng": str(poi.lng),
                      "icon": poi.icon.img.url if hasattr(poi.icon, "img") else "",
                      "desc_id": _get_poi_descs(poi),
                      "title": poi.title,
                      "date": str(poi.action_date.year) if poi.action_date else "2077", #change this to dynamic in future
                    })
  return poi_out


def _get_poi_descs(poi):
  descs = ""
  if poi.extra_desc:
    descs += poi.extra_desc
    descs += ";"
  if hasattr(poi.poi_desc, "id"):
    descs += ("%s,%s" % (poi.poi_desc.id, poi.poi_desc.title) )
  return descs.rstrip(';')
  
def _get_poi_for_event(events = None):
  if events:
    pois = []
    for event in events:
        event_obj = EventType.objects.get(name = event)
        pois += list(POI.objects.filter(event_type = event_obj))        
    return pois
  return list(POI.objects.all())


def _get_poi_for_factions(factions = None):
  if factions:
    pois = []
    for faction in factions:
        faction_obj = Faction.objects.get(name = faction)
        pois += list(POI.objects.filter(faction = faction_obj))        
    return pois
  return list(POI.objects.all())


def _get_poi_for_objects(objects = None):
  if objects:
    pois = []
    for obj in objects:
        obj_type = ObjectType.objects.get(name = obj)
        pois += list(POI.objects.filter(object_type = obj_type))        
    return pois
  return list(POI.objects.all())

def filter_poi(factions = None, objects = None, events = None):
  filters = {}
  if events:
    filters['event_type__name__in'] = events
  if factions:
    filters['faction__name__in'] = factions
  if objects:
    filters['object_type__name__in'] = objects
  return POI.objects.filter(**filters)        

@ensure_csrf_cookie
def get_poi_count(request):
  return HttpResponse(json.dumps({"count":POI.objects.count()}), content_type="application/json")

@ensure_csrf_cookie
def get_icon_list(request):
  list_out = []
  objs = Image.objects.filter(typ = "I")
  for obj in objs:
      list_out.append([obj.id, obj.img.url])
  return HttpResponse(json.dumps({"icons": list_out, "typ":"icons"}), content_type="application/json")

@ensure_csrf_cookie
def get_poi_icon(request):
  post = request.POST
  poi = POI.objects.get(id = post.get('id'))
  if poi.icon:
    return HttpResponse(json.dumps({"icon":[poi.icon.id, poi.icon.img.url]}), content_type="application/json")
  return HttpResponse('0')


@ensure_csrf_cookie
def get_poi_property_dialog(request):
  post = request.POST
  poi = None
  
  if(check_permissions(request, "map.add_poi", "map.change_poi")):
    return HttpResponse(json.dumps({"error":"No access to adding/editing poi"}), content_type="application/json")
  
  lat = post.get('lat')
  lng = post.get('lng')
  
  if post.get('id'):
    poi = POI.objects.get(id = post.get('id'))  
    lat = poi.lat
    lng = poi.lng
    
  output_data = {
                 "poi_property": None,
                 "obj_types": list(ObjectType.objects.all()),
                 "evt_types": list(EventType.objects.all()),
                 "factions": list(Faction.objects.all()),
                 "lat": lat,
                 "lng": lng,
                }
  
  if poi:
    output_data['poi_property'] = {
                                   "id": poi.id,
                                   "title": poi.title,
                                   "obj_type_id": poi.object_type.id if poi.object_type else None,
                                   "evt_type_id": poi.event_type.id if poi.event_type else None,
                                   "fct_id": poi.faction.id if poi.faction else None,
                                   "action_date": poi.action_date.strftime('%Y-%m-%d'),
                                   "icon": poi.icon if poi.icon else None,
                                   }  
    output_data['descriptions'], output_data['used_descriptions'] = _sort_used_and_unused_descriptions(poi)
  else:
    output_data['descriptions'] = list(Description.objects.all())
   
  return render(request, "poi-property-dialog.html", output_data)
  
  
  
def _sort_used_and_unused_descriptions(poi):
    descriptions = list(Description.objects.all())
    poi_descs = []
    used_descs = []
    if poi.poi_desc:
      poi_descs.append(poi.poi_desc.id)
    if poi.extra_desc:
      poi_descs += [x.split(',')[0] for x in poi.extra_desc.split(';')]
    
    for desc in list(descriptions):
      if str(desc.id) in poi_descs:
        used_descs.append(desc)
        descriptions.remove(desc)
    
    return (descriptions, used_descs)
    

  
@ensure_csrf_cookie
def save_poi(request):
  post = request.POST
  
  if(check_permissions(request, "map.add_poi", "map.change_poi")):
    return HttpResponse("0xa0")
  
  user_area = list(UserArea.objects.filter(user = request.user))
  
  in_area = False
  if not len(user_area):
    in_area = True
    
  for area in user_area:
    if in_sector(float(post.get('lat')), float(post.get('lng')), area.area):
      in_area = True
      break
      
  if not in_area:
    return HttpResponse(json.dumps({"error":"POI not in your area..."}), content_type = "application/json")
  
  try:
    faction = post.get('faction')
    obj_type = post.get('object_type')
    event_type = post.get('event_type')
    icon = Image.objects.get(id = post['icon']) if post.get('icon') else None
    if post.get('ppaid') == "None":
      poi = POI.objects.create(lat = post.get('lat'),
                     lng = post.get('lng'),
                     title = post.get('title', ""),
                     extra_desc = post.get('desc', ""),
                     action_date = post['actionDate'],
                     user = request.user,
                     )
      if icon:
        poi.icon = icon
      if faction and faction != 'null':
        poi.faction = Faction.objects.get(id = post['faction'])
      if obj_type and obj_type != 'null':
        poi.object_type = ObjectType.objects.get(id = post['object_type'])
      if event_type and event_type != 'null':
        poi.event_type = EventType.objects.get(id = post['event_type'])
      poi.save()
      add_log("Created new POI at %s %s" % (post.get('lat'), post.get('lng')), request.user)
      output = {
                "id":poi.id,
               }
      if poi.icon:
        output['icon'] = poi.icon.img.url
        
      return HttpResponse(json.dumps(output), content_type="application/json")
    else:  
      poi = POI.objects.get(id = post.get('ppaid'))
      poi.lat = Decimal(post.get('lat'))
      poi.lng = Decimal(post.get('lng'))
      if post.get('desc'):
        poi.extra_desc = post.get('desc', "")
      if icon:
        poi.icon = icon
      if post.get('actionDate'):
        poi.action_date = post['actionDate']
      if faction:
        poi.faction = Faction.objects.get(id = post['faction']) if faction != 'null' else None
      if obj_type:
        poi.object_type = ObjectType.objects.get(id = post['object_type']) if obj_type != 'null' else None
      if event_type:
        poi.event_type = EventType.objects.get(id = post['event_type']) if event_type != 'null' else None
      if post.get('title'):
        poi.title = post['title']
      poi.user = request.user
      poi.save()
      add_log("Edited POI #%s" % post.get('ppaid'), request.user)
  except Exception as e:
    return HttpResponse(json.dumps({"error":e.message}), content_type='application/json')
  return HttpResponse("saved")


@ensure_csrf_cookie
def remove_poi(request):
  post = request.POST
  ppaid = request.POST.get('ppaid')
  if not ppaid:
    return HttpResponse(json.dumps({"error":"Poi without id... try to reload the page and remove it again..."}))
  poi = POI.objects.get(id = int(ppaid))
    
  if(check_permissions(request,"map.delete_poi")):
    return HttpResponse('0xa0')
  
  user_area = list(UserArea.objects.filter(user = request.user))
  in_area = False
  if not len(user_area):
    in_area = True
  
    
  for area in user_area:
    if in_sector(float(poi.lat), float(poi.lng), area.area):
      in_area = True
      break
      
  if not in_area:
    return HttpResponse(json.dumps({"error":"POI not in your area..."}), content_type = "application/json")
  
  try:
    poi.delete()
    add_log("Removed POI #%s" % ppaid, request.user)
  except Exception as e:
    return HttpResponse(json.dumps({"error":e.message}), content_type='application/json')
  return HttpResponse('1')