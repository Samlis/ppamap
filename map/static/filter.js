$(function() { 
  
  var $filterBox = $('#filterBox');
  
  function initFilterBox() {
    $filterBox.hide();
    $('body').on('click', '.close-filter, button#showFilters', toggleFilter);
    $('.filter-data').on('click', filterObjects);
    $('#clearJoinedFilter').on('click', clearJoinedFilterObjects);
  }
  
  function clearJoinedFilterObjects() {
    $.ajax({
      url: "getjoinedfiltereddata",
      data: {
        "faction": "null",
        "event": "null",
        "object": "null"
      },
      type: "POST",
      success: reloadData
    });
  }
  
  function filterObjects() {
    var $factionInputs = $filterBox.find('#fraction input:checked');
    var $eventInputs = $filterBox.find('#event input:checked');
    var $objectType = $filterBox.find('#objectType input:checked');
    var data = {
      events: [],
      factions: [],
      objects: []
    };

    $eventInputs.each(function() {
      data['events'].push($(this).val());
    });

    $factionInputs.each(function() {
      data['factions'].push($(this).val());
    });

    $objectType.each(function() {
      data['objects'].push($(this).val());
    });

    addAjaxLoad($('.any-filter-title'));
    $.ajax({
      url:"getfiltereddata",
      data: data,
      type: "POST",
      success: reloadData,
      complete: removeAjaxLoad
    });
  }
  
  function reloadData(response) {
    
    if(response.hasOwnProperty('error')) {
      alert(response['error']);
      return
    }
      
    clearAllObjs();
    for(var i=0; i<response['poi'].length; i++)
      addToList( response['poi'][i].date,
                 ppamap.objList.poi,
                 ppamap.addPoi( response['poi'][i].lat,
                         response['poi'][i].lng,
                         response['poi'][i].title,
                         response['poi'][i].icon,
                         response['poi'][i].desc_id,
                         response['poi'][i].id
                        )
                );
    
    for(var i=0; i<response['poly'].length; i++)
      addToList(response['poly'][i].date,
                ppamap.objList.poly,
                ppamap.addPoly(
                  response['poly'][i].paths,
                  response['poly'][i].strokecolor,
                  response['poly'][i].strokeopt,
                  response['poly'][i].strokeweight,
                  response['poly'][i].fillcolor,
                  response['poly'][i].fillopt,
                  response['poly'][i].id,
                  response['poly'][i].poly_desc
                )
               );
    runDateFilter();
    toggleFilter();
    
  }
  
  function toggleFilter() {
    $filterBox.toggle('slide', {direction:'up'}, 500);
  }
  
  $(window).load(initFilterBox);
  
});