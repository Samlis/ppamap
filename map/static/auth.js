$('span#login').on('click', showLoginDialog);
$('span#logout').on('click', showLogoutDialog);

function showLogoutDialog() {
  $('<div></div>').dialog({
    modal: true,
    close: closeAction,
    buttons: [
              {
               text: "Cancel",
               click: closeDialog
               },
              {
               text: "Log me out!",
               click: logoutAction
               },
              ]
  });
}

function showLoginDialog() {
  $('<div id="login-dialog">'+createLoginDialog()+'</div>').dialog({
    modal: true,
    close: closeAction,
    buttons: [
              {
               text: "Login",
               click: loginUser
               },
              ]
  });
  $('div#login-dialog').dialog('open');
}

function loginUser() {
  var data = {
      "login": $('input#login').val(),
      "password": $('input#pwd').val()
  };
  $.ajax({
    type:"POST",
    url:"login",
    data:data,
    success: function(resp) {
      if(parseInt(resp))
        location.reload();
      else
        alert("No user, wrong password, user not active or smurfs took control of TV...");
    },
    failed: function(){
      alert("Login failed...")
    }
  });
}

function createLoginDialog() {
  var html = "<table class='dialog-table'>";
  html += "<tr><td>Login</td><td><input type='text' id='login'></td></tr>";
  html += "<tr><td>Password</td><td><input type='password' id='pwd'></td></tr></table>";
  return html
}

function logoutAction() {
  $.ajax({
    type:"POST",
    url:"logout",
    data:{'':''},
    success: function(resp) {
      if(parseInt(resp))
        location.reload();
      else
        alert('failed');
    }
  });
}