
var polyEdit = false;
var restrictedArea;
var restAreaData;

$(function(){ 
  
  $(window).load(function(){
      google.maps.event.addListener(ppamap.map, 
        "rightclick",
        function( evt ) {
          openContextMenu(evt.pixel.x, evt.pixel.y, 'creation', evt);
      }
    );
  });
  
});

// This needs serious refactoring to objective...

$('button#add-new-description').on('click', function() {
  window.open('../editor/new', "New Description", "height=500,width=900");
});

$('button#showMyRestrictions').on('click', showUserRestrictions);

function addEditDescAction() {
  $('button.edit-desc-button').on('click', function() { 
    window.open('../editor/'+$(this).attr('desc-id'), "Edit Description", "height=500,width=900");
  });
}

function creationMenu() {
  var menuDiv = "<ul class='context-menu'><li id='addpoi' class='context-menu-item'>Add POI</li>";
  menuDiv += "<li id='addpoly' class='context-menu-item'>Start Polygon</li></ul>";
  return menuDiv
}

function editPoiMenu() {
  var menuDiv = "<ul class='context-menu'><li id='move' class='context-menu-item'>Move</li>";
  menuDiv += "<li id='edit' class='context-menu-item'>Edit</li>";
  menuDiv += "<li id='remove' class='context-menu-item'>Remove</li></ul>";
  
  return menuDiv
}

function editPolyMenu() {
  var menuDiv = "<ul class='context-menu'><li id='editprops' class='context-menu-item'>Edit properties</li>";
  menuDiv += "<li id='editpoly' class='context-menu-item'>Edit polygons</li>";
  menuDiv += "<li id='remove' class='context-menu-item'>Remove</li></ul>";
  return menuDiv
}

function openContextMenu(x, y, type, evt, that) {
  if(!polyEdit){
    menuShow = true;
    var $menu = $('#contMenu');
    if($menu.css('display') == 'none')
      $menu.css('display','inline');
    $menu.css('left', x);
    $menu.css('top', y);
    if(type == "creation") {
      $menu.html(creationMenu());
      addCreationMenuActions(evt);
    }
    else if (type == "editpoi") {
      $menu.html(editPoiMenu());
      addEditPoiMenuActions(evt, that);
    }
    else if (type == "editpoly") {
      $menu.html(editPolyMenu());
      addEditPolyMenuActions(evt, that);
    }
  }
}

function addEditPolyMenuActions(evt, that){
  $('li#editprops').on('click', function() {
    hideMenu();
    editPolyPropertyDialog(evt, that);
  });
  
  $('li#editpoly').on('click', function() {
    var originalPaths = that.getPath().getArray().slice();
    hideMenu();
    polyEdit = true;
    that.setEditable(true);
    addButtonOnMap(['Save', 'Cancel']);
    $('#Save-editing').on('click', function() {
      data = {
          "poly":toNormalArray(that.getPath().getArray()),
          "id": that.ppaid
          };
      ajax("savepolygons", "POST", data,
        function(resp) {
          if(!resp.hasOwnProperty("error")) {
            that.setEditable(false);
            polyEdit = false;
            removeButtonOnMap(['Save','Cancel']);
          }
          else
            alert(resp['error']);
        });
    });
   $('#Cancel-editing').on('click', function() {
     that.setEditable(false);
     that.setPath(originalPaths);
     removeButtonOnMap(['Save','Cancel']);
     polyEdit = false;
   });
  });
  $('li#remove').on('click', function() {
    hideMenu();
    var confFunc = function() {
      removePoly(that);
      closeDialog($(this));
    };
    var cancFunc = function() {
      closeDialog($(this));
    };
    confirmDialog("Remove Polygon?", confFunc, "Yes, do it!", cancFunc, "NOOOOOO!!!!");
  });
}

function editPolyPropertyDialog(evt, that) {
  var data = {};
  if(that.ppaid)
    data['id'] = that.ppaid
  $.ajax({
    data: data,
    type: "POST",
    url: "getpolydialog",
    success: function(resp) {
      if(typeof(resp) != "string" && resp.hasOwnAttribute('error'))
        alert(resp['error'])
      else
        $('<div id="poly-edit"></div>').html(resp).dialog({
          width: 500,
          modal: true,
          close: closeDialogFunc,
          buttons: [
                    { 
                      text: "Save",
                      click: function(){
                        clickSavePoly(that,evt);
                      }
                    }
                   ]
        });
      $('div#poly-edit').dialog('open');
      $('ul.descriptions-list, ul.used-descriptions').sortable({
        connectWith: "div.description-box ul"
      });
    }
  })
}

function clickSavePoly(that, evt) {
  var date = $('#active-date').val();
  var data = {
      "id": that.ppaid,
      "desc": getSelectedDescriptions($('div#poly-edit')),
      "strokeColor": $('#stroke-color').val(),
      "strokeWeight": $('#stroke-weight').val(),
      "strokeOpacity": $('#stroke-opacity').val(),
      "fillColor": $('#fill-color').val(),
      "fillOpacity": $('#fill-opacity').val(),
      "object_type" : $('select.object-type option:selected').val(),
      "event_type" : $('select.event-type option:selected').val(),
      "faction" : $('select.faction option:selected').val(),
  };
  if(date)
    data['actionDate'] = date;

  var succAct = function(resp) {
    if(!resp.hasOwnProperty("error")){
      closeDialog($('div#poly-edit'));
      that.setOptions({
        "fillColor":data['fillColor'],
        "fillOpacity":data['fillOpacity'],
        "strokeColor":data['strokeColor'],
        "strokeWeight":data['strokeWeight'],
        "strokeOpacity":data['strokeOpacity']
      });
      if(data['desc']!='null')
        google.maps.event.addListener(that,
            'click',
            function() {
            get_desc(data['desc']);
        });
    } else {
      alert(resp['error']);
      that.setMap(null);
    }
  };
  ajax("savepolygon", "POST", data, succAct);
}

function removePoly(obj) {
  $.ajax({
    url: "removePoly",
    data: {"id":obj.ppaid},
    type: "POST",
    success: function(data) {
      if(parseInt(data))
        obj.setMap(null);
      else
        alert(data['error']);
    },
    failed: function(data) {
      alert('failed');
    }
  });
}

function addCreationMenuActions(evt) {
  $('li#addpoi').on('click', function(){
    hideMenu();
    createAddPoiDialog(evt);
  });
  $('li#addpoly').on('click', function(){
    hideMenu();
    setPolyAddMode(evt);
  });
}

function setPolyAddMode(evt) {
  var polyArr = [];
  var obj = undefined;
  polyEdit = true;
  var polyAddListener = google.maps.event.addListener(ppamap.map, 'click',
      function(evt) {
          var newP = {lat: evt.latLng.lat(), lng: evt.latLng.lng()};
        if(!obj) {
          polyArr.push(newP);
          obj = new google.maps.Polygon({
            paths: polyArr,
            strokeColor: '#ff0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#ff0000',
            fillOpacity: 0.35
          });
          obj.setEditable(true);
          obj.setMap(ppamap.map);
        }
        else {
          polyArr = obj.getPath().getArray();
          polyArr.push(newP);
          obj.setPath(polyArr);
        }
      });
  addButtonOnMap(['Save','Cancel']);
  $('#Cancel-editing').on('click', function(){
    if(obj) { 
      obj.setEditable(false);
      obj.setMap(null);
    }
    polyEdit = false;
    removeButtonOnMap(['Save','Cancel']);
    google.maps.event.removeListener(polyAddListener);
  });
  $('#Save-editing').on('click', function() {
    data = {
        "poly":toNormalArray(obj.getPath().getArray()),
        "strokeColor": "#ff0000",
        "strokeOpacity": "0.8",
        "strokeWeight": "2",
        "fillColor": "#ff0000",
        "fillOpacity": "0.35"
        };
    $.ajax({
      url: "savepolygon",
      type: "POST",
      data: data,
      success: function(resp) {
        if(!resp.hasOwnProperty("error")) {
          google.maps.event.removeListener(polyAddListener);
          obj.setEditable(false);
          polyEdit = false;
          removeButtonOnMap(['Save','Cancel']);
          obj.setMap(null);
          ppamap.addPoly(data['poly'], data['strokeColor'], data['strokeOpacity'], data['strokeWeight'], data['fillColor'], data['fillOpacity'], resp);
        }
        else
          alert(resp['error']);
        
      },
      failure: function() {
        alert('failed')
      },
    });

  });
}


function toNormalArray(poly) {
  var out = ""
  for(var i = 0; i < poly.length; i++)
    out+=poly[i].lat()+","+poly[i].lng()+";"
  return out
}


function addEditPoiMenuActions(evt, that){
  $('li#move').on('click', function() {
    var originalPosition = new google.maps.LatLng( that.position.lat(), that.position.lng());
    that.setDraggable(true);
    hideMenu();
    addButtonOnMap(['Save','Cancel']);
    $('#Save-editing').on('click', function() {
      var data = {
          "ppaid":that.ppaid,
          "lat": that.position.lat(),
          "lng": that.position.lng()
          };
      $.ajax({
        url: "poisave",
        type: "POST",
        data: data,
        success: function(data) {
          if(data == 'saved'){
            that.setDraggable(false);
            removeButtonOnMap(['Save','Cancel']);
          } else
            alert(data['error']);
        },
        failure: function() {
          alert('failed')
        },
      });

    });
    $('#Cancel-editing').on('click', function(){
      that.setDraggable(false);
      removeButtonOnMap(['Save','Cancel']);
      that.setPosition(originalPosition);
    });
  });
  $('li#remove').on('click', function() {
    hideMenu();
    var confFunc = function() {
      var data = {"ppaid":that.ppaid};
      $.ajax({
        url: "poiremove",
        type: "POST",
        data: data,
        success: function(data) {
          if(typeof(data) == "object")
            alert(data['error']);
          else 
            that.setMap(null);
        },
        failure: function() {alert('error removing poi...');},
      });
      closeDialog($(this));
    }
    var cancFunc = function() {
      closeDialog($(this));
    }
    confirmDialog("Remove Poi?", confFunc, "Yes, do it!", cancFunc, "Noooooo!!!");
  });
  $('li#edit').on('click', function() {
    hideMenu();
    createAddPoiDialog(evt, that);
  });
}

function createAddPoiDialog(evt, that) {
  var data = {
    lat: that ? that.position.lat() : evt.latLng.lat(),
    lng: that ? that.position.lng() : evt.latLng.lng()
  }
  
  if(that)
    data['id'] = that.ppaid
  
  $.ajax({
    url: "getpoidialog",
    type: "POST",
    data: data,
    success: function(resp) {
      if(typeof(resp) != "string" && resp.hasOwnAttribute('error'))
        alert(resp['error'])
      else
        $('<div id="add-poi-dialog">'+resp+'</div>').dialog({
          title: "Add Poi",
          modal: true,
          minWidth: 550,
          close: closeDialogFunc,
          buttons: [
                    {
                      text: "Save",
                      click: clickSavePoi
                    }
                   ]
        });
      $('#add-poi-dialog').dialog("open");
      $('ul.descriptions-list, ul.used-descriptions').sortable({
        connectWith: "div.description-box ul"
      });
      $('button.icons').on('click', function(){
        var iconDial = new imgChooser('poiiconlist', $(this), 'icon');
        iconDial.showDialog();
      })
    }
  });
}

function clickSavePoi(e) {
  var $that = $(this);
  var icon = $that.find('button.icons').attr('pic-id');
  var desc = getSelectedDescriptions($that);
  var obj = $that.find('select.object-type option:selected').val();
  var event = $that.find('select.event-type option:selected').val();
  var faction = $that.find('select.faction option:selected').val();
  var lat = $that.find('input.lat').val();
  var lng = $that.find('input.lng').val();
  var date = $that.find('input#active-date').val();
  var title = $that.find('input.title').val();
  var id = $that.find('table.dialog-table').attr('ppaid');  
  var data = {
      'title': title,
      'lat': lat,
      'lng': lng,
      'desc': desc,
      'object_type': obj,
      'faction': faction,
      'event_type': event,
      'ppaid': id ? id : "None",
  }
  if(date)
    data['actionDate'] = date;
  if(icon)
    data['icon'] = icon
  $.ajax({
    url: "poisave",
    type: "POST",
    data: data,
    success: function(resp) {
      if(typeof(resp) == "object") {
        if(resp.hasOwnProperty('error')) {
          alert(resp['error']); 
        }
        else if(resp.hasOwnProperty('id')) {
          ppamap.addPoi(lat, lng, title, resp['icon'], desc, resp['id']);
          closeDialog($('#add-poi-dialog'));
        }
      }
      else if(resp == 'saved') {
        closeDialog($('#add-poi-dialog'));
      }
    },
    failure: function(resp) { 
      alert('error in loading new id...'); 
    },
  });
}

function confirmDialog(title, confirmFunction, confirmText, cancelFunction, cancelText) {
  $('<div id="confirm-dialog"></div>').dialog(
      {
        title: title,
        width: 300,
        modal: true,
        close: closeDialogFunc,
        buttons: [
                  { 
                    text: confirmText,
                    click: confirmFunction
                  },
                  {
                    text: cancelText,
                    click: cancelFunction
                  }
                 ]
      }    
      );
}

function closeDialogFunc(e) {
  closeDialog($(this));
}

function closeDialog($that) {
  $that.dialog('close');
  $that.remove();
}

function ajax(url, typ, data, succ, fail) {
  var ajaxData = {
    url: url,
    type: typ,
    data: data,
    success: succ ? succ : defaultSuccess,
    failure: fail ? fail : defaultFailure,
  };
  $.ajax(ajaxData);
}

function defaultFailure(response) {
  alert("FAILED!!!! WE ARE ALL GONNA DIE!!!");
}

function defaultSuccess(response) {
  alert("it was okay...");
}

function addButtonOnMap(args) {
  var $divMap = $('div#map-box');
  for(var i=0; i<args.length; i++)
    $divMap.append("<button class='map-btn' id='"+args[i]+"-editing' style='position:absolute; left:"+(57*i)+"; top:0; z-index:101;'>"+args[i]+"</button>");
}

function removeButtonOnMap(args) {
  for(var i=0; i<args.length; i++)
    $('button#'+args[i]+'-editing').remove();
}

function getSelectedDescriptions($that) {
  var $li = $that.find('ul.used-descriptions li');
  var descOut = []
  $li.each(function(){
    descOut.push($(this).attr('value') +","+$(this).html()); 
  });
  return descOut.join(';')
}

// restriction area

function showUserRestrictions() {
  if(!restAreaData) {
    $.ajax({
      url:"getuserarea",
      type:"GET",
      success: function(resp) {
        if(resp.hasOwnProperty("area")) {
          restAreaData = [];
          for(var i = 0; i < resp['area'].length; i++) 
            restAreaData.push(polyStrToArray(resp['area'][i]));
          showUserRestrictions();
        }
        else
          alert(resp['error']);
      },
      failure: function() { alert("Failed to retrieve data"); }
    })
    return
  }
  
  if(restrictedArea) {
    restrictedArea.setMap(null);
    restrictedArea = undefined;
    $('button#showMyRestrictions').html('Show my area');
  } else {
    if(!restrictedArea) {
      restrictedArea = new google.maps.Polygon({
        paths: restAreaData,
        fillColor: '#aa0000',
        fillOpacity: 0.85,
        strokeWeight: 2,

      });
    }
    restrictedArea.setMap(ppamap.map);
    $('button#showMyRestrictions').html('Hide my area');
  }
}



