$(function(){
  
  var $informationBox = $('div#informationBox');
  
  function onInit(){
    $informationBox.hide();
    $('body').on('click', '.info-close-button, button#about', toggleBox);
  }
  
  function toggleBox() {
    $informationBox.toggle('slide', {direction:'up'}, 500);
  }
  
  $(window).load(onInit);
  
});