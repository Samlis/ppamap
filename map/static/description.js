$(function() {
  
  var $textBox = $('#textBox');
  var $textToggler = $('#textToggler');
  
  function initDescription() {
    $textBox.hide();
    $textToggler.on('click', toggleText);
    $textBox.on('click', '.hide-text-box', toggleText);
  }
  
  function toggleText() {
    $textBox.toggle('slide', {direction: "up"}, 500);
    var $span = $textToggler.find('span');
    if($span.hasClass('ui-icon-triangle-1-s'))
      $span.removeClass('ui-icon-triangle-1-s').addClass('ui-icon-triangle-1-n');
    else
      $span.removeClass('ui-icon-triangle-1-n').addClass('ui-icon-triangle-1-s');
  }
  
  $(window).load(initDescription);
  
});