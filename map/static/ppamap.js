class PPAMapView {
  
  constructor() {
    this.objList = { poi: {}, poly: {} };
    this.$map = $('#map-box');
    this.map = undefined;
  }
  
  init() {
    $.ajax({
      url: '/get_data',
      type: "POST",
      data: {'id': 'none'},
      context: this,
    })
    .done(this.dataDownloadSuccess)
    .fail(onAjaxFail);
  }
  
  dataDownloadSuccess(resp) {
    var that = this;
    $.each(resp.pois, function(idx, poi) {
      PPAMapView.addToList(poi.date, that.objList.poi, that.addPoi(poi.lat, poi.lng, poi.title, poi.icon, poi.desc_id, poi.id ));
    });
    $.each(resp.polys, function(idx, poly) {
      PPAMapView.addToList(poly.date, that.objList.poly, that.addPoly(poly.paths, poly.strokecolor, poly.strokeopt, poly.strokeweight, poly.fillcolor, poly.fillopt, poly.id, poly.poly_desc));
    });
    initYearFilter();
  }
  
  static addToList(year, list, obj) {
    if( list[year] === undefined)
      list[year] = [];
    list[year].push(obj);
  }
  
  get_desc(descId) {
    $.ajax({
      url: "desc/" + descId,
      type: "GET",
      context: this,
      success: this.get_desc_success,
      failure: onAjaxFail,
    });
  }
  
  get_desc_success(resp) {
    var $textBox = $('#textBox')
    $textBox.find('.text-box-inside').html( createDescriptionDiv(resp) ); 
    $textBox.show('slide', { direction:"up" }, 500);
    $('#textToggler span').removeClass('ui-icon-triangle-1-s').addClass('ui-icon-triangle-1-n');
    if(loggedin) 
      addEditDescAction();
  }
  
  addPoi(lat, lng, title, icon, descId, poiId) {
    var obj_data = {
                    position: new google.maps.LatLng( lat, lng ),
                    map: ppamap.map,
                    title: title 
                    }
    if(icon && icon.indexOf("null") == -1)
      obj_data['icon'] = icon
      
    var obj = new google.maps.Marker(obj_data);
    google.maps.event.addListener(obj,
      'click',
      function(a, b, c, d, e, f) {
        showDescriptionMenuOrShowDescription(event.clientX, event.clientY, descId.split(';') );
      });
    if(loggedin)
      google.maps.event.addListener(obj,
        "rightclick",
        function( evt ) {
          openContextMenu(event.clientX, event.clientY, 'editpoi', evt, this);
        });
    if(poiId)
      obj['ppaid'] = poiId;
    return obj;
  }
  
  addPoly(poly, strokeColor, strokeOpacity, strokeWeight, fillColor, fillOpacity, polyId, descId) {
    var obj = new google.maps.Polygon({
      paths: polyStrToArray(poly),
      strokeColor: strokeColor,
      strokeOpacity: strokeOpacity,
      strokeWeight: strokeWeight,
      fillColor: fillColor,
      fillOpacity: fillOpacity
    });
    
    google.maps.event.addListener(obj,
        'click',
        function() {
          showDescriptionMenuOrShowDescription(event.clientX, event.clientY, descId.split(';') );
        });
    if(loggedin)
      google.maps.event.addListener(obj,
          "rightclick",
          function( evt ) {
            openContextMenu(event.clientX, event.clientY, 'editpoly', evt, this);
          });
    if(polyId)
      obj['ppaid'] = polyId;
    obj.setMap(ppamap.map);
    return obj
  }

  
}