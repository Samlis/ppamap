$(function() {
  
  function setupAdminActions() {
    var $restrictionButton =  $('button#adminAddRestrictionArea');
    
    $restrictionButton.on('click', startAddRestrictionAreaMode);
    
  }
  
  
  function startAddRestrictionAreaMode() {
    if(polyEdit)
      return
    polyEdit = true;
    var polyArr = [];
    var obj = undefined;
    var polyAddListener = google.maps.event.addListener(ppamap.map, 'click',
        function(evt) {
            var newP = {lat: evt.latLng.lat(), lng: evt.latLng.lng()};
          if(!obj) {
            polyArr.push(newP);
            obj = new google.maps.Polygon({
              paths: polyArr,
              strokeColor: '#ff0000',
              strokeOpacity: 0.8,
              strokeWeight: 2,
              fillColor: '#ff0000',
              fillOpacity: 0.35
            });
            obj.setEditable(true);
            obj.setMap(ppamap.map);
          }
          else {
            polyArr = obj.getPath().getArray();
            polyArr.push(newP);
            obj.setPath(polyArr);
          }
        });
    addButtonOnMap(['Save','Cancel']);
    addUserSelector();
    $('#Cancel-editing').on('click', function(){
      if(obj) {
        obj.setEditable(false);
        obj.setMap(null);
      }
      polyArr = [];
      polyEdit = false;
      removeButtonOnMap(['Save','Cancel']);
      $('#userSelection').remove();
      google.maps.event.removeListener(polyAddListener);
    });
    
    $('#Save-editing').on('click', function() {
      data = {
          "poly":toNormalArray(obj.getPath().getArray()),
          "user": $('#userSelection option:selected').val(),
          };
      $.ajax({
        url: "saverestrictionarea",
        type: "POST",
        data: data,
        success: function(resp) {
          if(resp == "1") {
            $('#userSelection').remove();
            removeButtonOnMap(['Save','Cancel']);
            obj.setMap(null);
            obj.setEditable(false);
            polyEdit = false;
            google.maps.event.removeListener(polyAddListener);
            addPoly(data['poly'], data['strokeColor'], data['strokeOpacity'], data['strokeWeight'], data['fillColor'], data['fillOpacity'], resp);
          }
          else {
            alert(resp['error']);
          }
        },
        failure: function() {
          alert('failed')
        },
      });

    });  
  }
  
  function addUserSelector() {
    var $divMap = $('div#map-box');
    $divMap.append('<select id="userSelection" class="on-map-item" style="position: absolute; left: 137px; font-size: 16pt; font-family: monospace; top: 0; z-index: 101;"></select>');
    $.ajax({
      url: "getUserNames",
      type: "GET",
      success: function(response) {
        var $select = $('#userSelection');
        for(var i = 0; i < response['usr'].length; i++)
          $select.append("<option value='"+response['usr'][i][0]+"'>"+response['usr'][i][1]+"</option>");
      },
      failure: function() {
        alert('Failed getting users');
      }
    })
  };
  
  $(window).load(setupAdminActions);
  
});