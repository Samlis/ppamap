// This needs serious refactoring to objective...
$('button#runFilter').on('click', runDateFilter);
var showYears = [];
var $mapBox = $('#map-box');
var menuShow = false;
var MAP_ID = 'PPAMAP';
var mapOptions = {
    center: { lat: 52.397, lng: 18.444},
    zoom: 7,
    mapTypeId: MAP_ID,
    disableDefaultUI: true,
    mapTypeControl: true,
    panControl: true,
    scaleControl: true,
    streetViewControl: false,
    mapTypeControlOptions: {
      mapTypeIds: [MAP_ID, 'satellite'],
      position: google.maps.ControlPosition.TOP_RIGHT,
    }
  };
// need to add my custom map;
var mapStyle = [
                {
                  "stylers": [
                    { "hue": "#ff6e00" }
                  ]
                },{
                  "featureType": "water",
                  "stylers": [
                    { "hue": "#007fff" }
                  ]
                },{
                  "featureType": "road.highway",
                  "stylers": [
                    { "hue": "#66ff00" },
                    { "saturation": -100 },
                    { "visibility": "on" }
                  ]
                },{
									"featureType": "landscape.natural",
									"elementType": "geometry.fill",
									"stylers": [
										{ "hue": "#ffa200" },
										{ "saturation": -46 },
										{ "visibility": "on" }
									]
								},{
									"featureType": "poi.park",
									"elementType": "geometry.fill",
									"stylers": [
										{ "visibility": "on" },
										{ "hue": "#ffa200" },
										{ "saturation": -46 }
									]
								},{
									"featureType": "water",
									"elementType": "geometry.fill",
									"stylers": [
										{ "visibility": "on" },
										{ "hue": "#0000ff" },
										{ "saturation": -26 }
									]
								}
              ];

var styledMapOptions = {
    name: "PPAMAP"
}

$(function() {
  ppamap = new PPAMapView();
  
  function initMap() {
   ppamap.init();
   ppamap.$map.css('height', ($(window).height() - 50) );
   ppamap.$map.css('width', $(window).width() );
   
   ppamap.map = new google.maps.Map(ppamap.$map.get(0), mapOptions);
   var customMap = new google.maps.StyledMapType(mapStyle, styledMapOptions);
   ppamap.map.mapTypes.set(MAP_ID, customMap);
   showAllPoiAt(ppamap.map);
   google.maps.event.addListener(ppamap.map, 
        "click",
        function( event ) {
          if(typeof menuShow !== "undefined" && menuShow)
            hideMenu();
      }
    );
  }
  
  $(window).load(initMap);
});

function onAjaxFail(resp) {
  console.log('failed');
}



function showDescriptionMenuOrShowDescription(x, y, descriptions) {
  menuShow = true;
  if(descriptions.length == 1 && descriptions[0].length) {
    ppamap.get_desc(descriptions[0].split(',')[0]);
  } else if( descriptions.length > 1) {
    var $menu = $('#contMenu');
    if($menu.css('display') == 'none')
      $menu.css('display','inline');
    $menu.css('left', x);
    $menu.css('top', y);
    $menu.html(createMenuFromArray(descriptions));
    addDescriptionMenuActions();
  }
}

function addDescriptionMenuActions() {
  var $items = $('#contMenu li.list-menu-item');
  $items.on('click', onDescItemClick);
}

function onDescItemClick() {
  ppamap.get_desc($(this).attr('value'));
}

function createMenuFromArray(list) {
  var html = "<ul class='list-menu'>";
  for(var i = 0; i < list.length; i++) {
    var element = list[i].split(',');
    html += "<li class='list-menu-item' value='"+element[0]+"'>"+element[1]+"</li>";
  }
  html += "</ul>";
  return html
}

function createDescriptionDiv(data) {
  var div = '<div class="up-bar">'+data['title'];
  if(loggedin)
    div += "<button class='edit-desc-button' desc-id='"+data['id']+"'><span class='ui-icon ui-icon-pencil'></span></button>";
  div += "<button class='hide-text-box'>X</button>";
  div += '</div>';
  div += '<p class="text">'+data['desc']+'</p>'
  return div;
}


function parseStringToLatLng(data){
  var latlng = data.split(',');
  return new google.maps.LatLng( parseFloat(latlng[0]), parseFloat(latlng[1]) );
}


function polyStrToArray(data){
  var polyArray = [];
  var polyData = data.split(';');
  polyData.pop();
  for(var i=0; i < polyData.length; i++) {
      polyArray.push( parseStringToLatLng(polyData[i]) );
  }
  return polyArray
}

function closeAction() {
  $(this).dialog('close');
  $(this).remove();
}


function runDateFilter() {
  var $from = $('#fromYear');
  var $to = $('#toYear');

  if( !validInputField($to) || !validInputField($from) )
    return
    
  showYears = getYearList(parseInt($from.val()), parseInt($to.val())+1);
  var yearsPoly = Object.keys(ppamap.objList.poly);

  showAllPoiAt(null);
  
  for(var i = 0; i < yearsPoly.length; i++)
    changeElementsMapFromYear(yearsPoly[i], null, ppamap.objList.poly);
  
  for(var i = 0; i < showYears.length; i++) {
    changeElementsMapFromYear(showYears[i], ppamap.map, ppamap.objList.poi);
    changeElementsMapFromYear(showYears[i], ppamap.map, ppamap.objList.poly);
  }
}


function showAllPoiAt(mapData) {
  var yearsPoi = Object.keys(ppamap.objList.poi);
  for(var i = 0; i < yearsPoi.length; i++)
    if(showYears.length == 0 || showYears.indexOf(yearsPoi[i]) > -1 || mapData == null)
      changeElementsMapFromYear(yearsPoi[i], mapData, ppamap.objList.poi);
}

function clearAllObjs() {
  var yearsPoi = Object.keys(ppamap.objList.poi);
  var yearsPoly = Object.keys(ppamap.objList.poly);
  
  for(var i = 0; i < yearsPoi.length; i++)
    changeElementsMapFromYear(yearsPoi[i], null, ppamap.objList.poi);
  
  for(var i = 0; i < yearsPoly.length; i++)
    changeElementsMapFromYear(yearsPoly[i], null, ppamap.objList.poly);
  
  ppamap.objList.poi = {};
  ppamap.objList.poly = {};
}


function changeElementsMapFromYear(year, mapData, list) {
  if(list[year] === undefined)
    return
  for(var i = 0; i < list[year].length; i++)
    list[year][i].setMap(mapData);
}

function addToList(year, list, obj) {
  if(typeof list[year] === "undefined")
    list[year] = [];
    list[year].push(obj);
 }

function getYearList(from, to) {
  var outYears = [];
  for(var i = from; i < to; i++)
    outYears.push(i+"");
  
  return outYears
}


function validInputField(element) {
  var type = element.attr('type');
  var val = element.attr('val');
  var ret = true;
  if(type == "number") {
    var min = element.attr('min') ? element.attr('min') : "0";
    var max = element.attr('max') ? element.attr('max') : "max";
    if(!isNaN(val) && parseInt(val) < parseInt(min) && (max != "max" || parseInt(val) > parseInt(max)) )
      ret = false;
  }
  return ret
}

//
// filter stuff
//

function initYearFilter() {
  var $rangeDiv = $('#rangeDiv');
  var $fromYear = $('div#rangeFilter #fromYear');
  var $toYear = $('div#rangeFilter #toYear');
  var values = getMinMaxYear();
  $rangeDiv.slider({
    range: true,
    values: values,
    max: values[1],
    min: values[0],
    slide: function(event, ui) {
      $fromYear.val(ui.values[0]);
      $toYear.val(ui.values[1]);
    }
  });
  $fromYear.val($rangeDiv.slider("values", 0));
  $toYear.val($rangeDiv.slider("values", 1));
}
  
function getMinMaxYear() {
  var years = Object.keys(ppamap.objList.poly).concat(Object.keys(ppamap.objList.poi)).sort();
  if(years.length)
    return [parseInt(years[0]), parseInt(years[years.length-1])]
  return [0,0]
}

function hideMenu() {
  menuShow = false;
  $('#contMenu').css('display', 'none');
}

function addAjaxLoad($obj) {
  var $spinner = $('<span class="ajax-load"></span>');
  $obj.append($spinner);
}

function removeAjaxLoad() {
  $('span.ajax-load').remove();
}