$(function(){
  
  function initCookie() {
    $('.close-cookies').on('click', closeCookieBox);
  }
  
  function closeCookieBox(){
    $('div.cookieBox').hide();
  }
  
  $(window).load(initCookie);
  
});