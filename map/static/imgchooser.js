var imgChooser = function(url, returnObj, typ) {
  this.url = url;
  this.returnToObj = returnObj;
  this.typ = typ;
  this.closeAct = closeDlgAction;
}

function closeDlgAction(){
  $(this).dialog('close');
  $(this).remove();
}

imgChooser.prototype.changeCloseAction = function(func) {
  this.closeAct = func;
};


imgChooser.prototype.showDialog = function() {
  var that = this;
  $('<div id="picChooserDialog"></div>').html('<div class="list" style="height:300px; max-height:500px"></div>').dialog({
    modal: true,
    width:700,
    title: "Image chooser",
    close: this.closeAct,
    buttons:[
              { 
                text: "Add choosen image",
                click: function() {
                  that.confirmFunction();
                  $(this).dialog('close');
                  $(this).remove();
                }
              },
              { 
                text: "Cancel",
                click: this.closeAct
              },
             ]
  });
  $('div#picChooserDialog').dialog('open');
  that.getPicList();
};

imgChooser.prototype.confirmFunction = function() {
  var selectedImg = $('span.pic-active');
  if(!selectedImg.length)
    return
    
  var style = {
        'background-image': selectedImg.css('background-image'),
      };  
  this.returnToObj.html("<span id='iconbox'></span>");
  this.returnToObj.find('span#iconbox').css(style);
  this.returnToObj.attr('pic-id', selectedImg.attr('obj-id'));
};


imgChooser.prototype.getPicList = function() {
  $.ajax({
    url: this.url,
    type: "POST",
    data: {"typ":this.typ},
    success: addPictureListToPicDiv,
    failure: function() { alert('failed on request for icons'); }
  });
};

function addPictureListToPicDiv(respList) {
  if(respList) {
    var objName = respList['typ'] == 'icons' ? 'icons' : "images";
    var $divList = $('div.list');
    for(var i=0; i<respList[objName].length; i++) {
      var id = respList[objName][i][0];
      var url = respList[objName][i][1];
      $divList.append('<span obj-id='+id+' class="box '+ objName +' pic-clickable" style="background-image:url('+url+');"></span>');
    }
    addPicBoxActions();
  }
  else
    alert('failed to load pic chooser');
}


function addPicBoxActions() {
  var $spans = $('div.list span.pic-clickable');
  $spans.on('click', setActive);
}


function setActive() {
  $('span.pic-active').removeClass('pic-active');
  $(this).addClass('pic-active');
}