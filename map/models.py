from django.db import models
from cStringIO import StringIO
from django.core.files.uploadedfile import SimpleUploadedFile
from django.contrib.auth.models import User
from django.utils import timezone


class EventType(models.Model):
  name = models.CharField(max_length=255, unique=True)
  
  def __unicode__(self):
    return self.name

  
class Faction(models.Model):
  name = models.CharField(max_length=255, unique=True)
  icon = models.ForeignKey("map.Image", blank=True, null=True)
  
  def __unicode__(self):
    return self.name


class ObjectType(models.Model):
  name = models.CharField(max_length=255, unique=True)
  
  def __unicode__(self):
    return self.name


class Description(models.Model):
  title = models.CharField(max_length=255, default="title")
  type = models.CharField(max_length=64, default="poi")
  author = models.CharField(max_length=100, blank=True, null=True)
  user = models.ForeignKey(User, blank=True, null=True)
  description = models.TextField(blank=True, null=True)
  creation_date = models.DateTimeField(default=timezone.now)
  modification_date = models.DateTimeField(default=timezone.now)
  verbose_name = "POI Description"
  verbose_name_plural = "POI Descriptionss"
  
  def __unicode__(self):
    return self.title
  
class Polygons(models.Model):
  user = models.ForeignKey(User, blank=True, null=True)
  poly_desc = models.ForeignKey(Description, blank=True, null=True)
  extra_desc = models.TextField(blank=True, null=True)
  stroke_color = models.CharField(max_length=7, help_text="only # format: #rrggbb")
  stroke_opacity = models.DecimalField(max_digits=3, decimal_places=2)
  stroke_weight = models.IntegerField(default=1)
  action_date = models.DateField(auto_now=False, blank=True, null=True)
  fill_color = models.CharField(max_length=7, help_text="only # format: #rrggbb")
  fill_opacity = models.DecimalField(max_digits=3, decimal_places=2)
  faction = models.ForeignKey(Faction, blank=True, null = True)
  event_type = models.ForeignKey(EventType, blank=True, null=True)
  object_type = models.ForeignKey(ObjectType, blank=True, null=True)
  paths = models.TextField()
  creation_date = models.DateTimeField(default=timezone.now)
  modification_date = models.DateTimeField(default=timezone.now)
  verbose_name = "POLYGON"
  verbose_name_plural = "POLYGONS"
  
  def __unicode__(self):
    if self.poly_desc:
      return self.poly_desc.title
    return str(self.pk)
  
class Image(models.Model):
  IMAGE_TYPES = (
                 ('I', 'POI Icon'),
                 ('P', 'Picture'),
                 )
  
  name = models.CharField(max_length=128, unique=True)
  img = models.ImageField(upload_to="image")
  thumbnail = models.ImageField(upload_to="image/thumb/", max_length=100, blank=True,null=True)
  typ = models.CharField(max_length=1, choices=IMAGE_TYPES, default='P')
  
  def __unicode__(self):
    return self.name

  def create_thumbnail(self):
    from PIL import Image
    # original code for this method came from
    # http://snipt.net/danfreak/generate-thumbnails-in-django-with-pil/
    if not self.img:
        return
    
    if self.typ == "I":
      return
    
    import os
    if self.img.height > self.img.width:
      THUMBNAIL_SIZE = (128,192)
    else:
      THUMBNAIL_SIZE = (192,128)
    
    DJANGO_TYPE = self.img.file.content_type
    
    if DJANGO_TYPE == 'image/jpeg':
        PIL_TYPE = 'jpeg'
        FILE_EXTENSION = 'jpg'
    elif DJANGO_TYPE == 'image/png':
        PIL_TYPE = 'png'
        FILE_EXTENSION = 'png'
        
    
    image = Image.open(StringIO(self.img.read()))
    image.thumbnail(THUMBNAIL_SIZE, Image.ANTIALIAS)
    
    temp_handle = StringIO()
    image.save(temp_handle, PIL_TYPE)
    temp_handle.seek(0) 
    
    suf = SimpleUploadedFile(os.path.split(self.img.name)[-1],
            temp_handle.read(), content_type=DJANGO_TYPE)
    self.thumbnail.save('%s_thumbnail.%s'%(os.path.splitext(suf.name)[0],FILE_EXTENSION), suf, save=False)

  def save(self):
    self.create_thumbnail()
    if self.typ == "I" and (self.img.height > 32 or self.img.width > 32):
      return False
    super(Image, self).save()
    
    
class POI(models.Model):
  user = models.ForeignKey(User, blank=True, null=True)
  title = models.CharField(max_length=255, default="title")
  poi_desc = models.ForeignKey(Description, blank=True, null=True)
  extra_desc = models.TextField(blank=True, null=True)
  icon = models.ForeignKey(Image, blank=True, null=True)
  lat = models.DecimalField(max_digits=12, decimal_places=9)
  lng = models.DecimalField(max_digits=12, decimal_places=9)
  action_date = models.DateField(auto_now=False, blank=True, null=True)
  creation_date = models.DateTimeField(default=timezone.now)
  modification_date = models.DateTimeField(default=timezone.now)
  faction = models.ForeignKey(Faction, blank=True, null=True)
  event_type = models.ForeignKey(EventType, blank=True, null=True)
  object_type = models.ForeignKey(ObjectType, blank=True, null=True)
  verbose_name = "POI"
  verbose_name_plural = "POIs"
  
  def __unicode__(self):
    return self.title

class UserArea(models.Model):
  user = models.ForeignKey(User)
  area = models.TextField(blank=True, null=True)
