from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.http import *
from django.views.decorators.csrf import ensure_csrf_cookie


def handler404(request):
  render(request, '404.html')

@ensure_csrf_cookie
def user_login(request):
  user = request.POST.get('login')
  pswd = request.POST.get('password')
  user_auth = authenticate(username=user, password=pswd)
  if user_auth:
    if user_auth.is_active:
      login(request, user_auth)
      return HttpResponse('1')
    else:
      return HttpResponse('0')
  else:
    return HttpResponse('0')

def user_logout(request):
  logout(request)
  return HttpResponse('1')
