from django.contrib import admin
from models import Image, POI, Description, Polygons, Faction, EventType, UserArea
from map.models import ObjectType

class ImageAdminForm(admin.ModelAdmin):
  list_display = ('name',)
  

class POIAdminForm(admin.ModelAdmin):
  list_display = ('title', 'poi_desc', 'lat', 'lng', 'object_type', 'faction', 'event_type')
  
  
class DescAdminForm(admin.ModelAdmin):
  list_display = ('title', 'author')
  list_filter = ('author',)
  

class PolygonAdminForm(admin.ModelAdmin):
  list_display = ('poly_desc', 'starts_at', 'object_type', 'faction', 'event_type')
  
  def starts_at(self, obj):
    if ';' in obj.paths:
      return obj.paths.split(';')[0]
    return ""
  
  starts_at.short_description = "Polygon starts at"


class FactionAdminForm(admin.ModelAdmin):
  list_display = ('name',)


class EventTypeForm(admin.ModelAdmin):
  list_display = ('name',)


class ObjectTypeForm(admin.ModelAdmin):
  list_display = ('name',)

class UserAreaForm(admin.ModelAdmin):
  list_display = ('user', 'area')

admin.site.register(EventType, EventTypeForm)
admin.site.register(Faction, FactionAdminForm)
admin.site.register(ObjectType, ObjectTypeForm)
admin.site.register(Image, ImageAdminForm)
admin.site.register(POI, POIAdminForm)
admin.site.register(Polygons, PolygonAdminForm)
admin.site.register(Description, DescAdminForm)
admin.site.register(UserArea, UserAreaForm)
