from models import Description, POI, Polygons, Image
from ppamap.security_utils import check_permissions
from django.http.response import HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie
import json
from map.models import ObjectType, EventType, Faction
from ppalogger.logger import add_log

def get_description(request, id):
  desc = Description.objects.get( pk = id )
  return HttpResponse(json.dumps({ "desc": desc.description, "author": desc.author, "title": desc.title, 'id': desc.id }), content_type="application/json")


@ensure_csrf_cookie
def get_all_option_ids(request):
  data_out = {
              'descs':[],
              'objs':[],
              'events':[],
              'factions':[],
              }
  all_desc = list(Description.objects.all())
  all_obj = list(ObjectType.objects.all())
  all_event = list(EventType.objects.all())
  all_faction = list(Faction.objects.all())
  id = request.POST.get('id')
  obj_desc = None
  obj_type_id = None
  obj_faction = None
  obj_event = None
  if id != "none":
    map_obj = POI.objects.get(id = id) if request.POST.get('typ') == "poi" else Polygons.objects.get(id = id)
    obj_desc =  map_obj.poi_desc if request.POST.get('typ') == "poi" else map_obj.poly_desc
    obj_type_id = map_obj.object_type.id if map_obj.object_type else None
    obj_faction = map_obj.faction.id if map_obj.faction else None
    obj_event = map_obj.event_type.id if map_obj.event_type else None
    
  for desc in all_desc:
    data = { 
             "id":desc.id,
             "title":str(desc.id)+". "+desc.title[0:32]
          }
    if obj_desc and obj_desc.pk == desc.id:
      data['selected'] = True
    data_out['descs'].append(data)
  
  data_out['objs'] = _parse_types_to_select_data(all_obj, obj_type_id)
  data_out['factions'] = _parse_types_to_select_data(all_faction, obj_faction)
  data_out['events'] = _parse_types_to_select_data(all_event, obj_event)
  
  return HttpResponse(json.dumps(data_out), content_type="application/json")
  
  
def _parse_types_to_select_data(data, type_id = None):
  out = []
  for obj in data:
    try:
      data = {
              "id":obj.id,
              "title":str(obj.id)+". "+obj.name[0:32]
              }
      if type_id and type_id == obj.id:
        data['selected'] = True
      out.append(data)
    except:
      continue
  return out


@ensure_csrf_cookie
def get_all_desc_ids(request):
  all_desc = list(Description.objects.all())
  data_out = []
  id = request.POST.get('id')
  obj_desc = None
  
  if id != "none":
    obj_desc = POI.objects.get(id = id).poi_desc if request.POST.get('typ') == "poi" else Polygons.objects.get(id = id).poly_desc

  for desc in all_desc:
    try:
      data = { 
               "id":desc.id,
               "title":str(desc.id)+". "+desc.title[0:32]
            }
      if obj_desc and obj_desc.pk == desc.id:
        data['selected'] = True
      data_out.append(data)
    except Exception as e:
      continue
  
  return HttpResponse(
                      json.dumps({"descs":data_out}),
                      content_type="application/json"
                      )


@ensure_csrf_cookie
def get_picture_list(request):
  if(check_permissions(request, "map.add_description", "map.change_description")):
    return HttpResponse("0x0a")
  try:
    list_out = []
    objs = Image.objects.filter(typ = "P")
    for obj in objs:
      list_out.append([obj.id, obj.thumbnail.url])
  except:
    return HttpResponse("0")
  list_out *= 5;
  return HttpResponse(json.dumps({"images": list_out, "typ":"images"}), content_type="application/json")


@ensure_csrf_cookie
def save_desc(request):
  post = request.POST
  if(check_permissions(request, "map.add_description", "map.change_description")):
    return HttpResponse("0x0a")
  desc = None
  try:
    if post.get('id') and post['id'] != 'none':
      desc = Description.objects.get( pk = post['id'] )
      if post.get('title'):
        desc.title = post['title']
      if post.get('desc'):
        desc.description = _remove_scripts(post['desc'])
      desc.user = request.user
      desc.save();
      add_log("Edited description #%s" % post.get('id'), request.user)
    else:
      desc = Description.objects.create(
                                 description = _remove_scripts(post.get('desc')),
                                 author = request.user.username,
                                 title = post.get('title'),
                                 user = request.user,
                                 )
      add_log("Added description #%s" % desc.id , request.user)
    return HttpResponse(desc.id)
  except Exception as e:
    return HttpResponse("error")
  

def _remove_scripts(txt):
  return txt.replace('<script', '')

def get_desc_obj(id):
  return Description.objects.get(id = int(id))

  