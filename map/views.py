from django.shortcuts import render
from models import POI, Polygons, Faction, EventType, ObjectType as obj_type
import poi 
import poly
from django.views.decorators.csrf import ensure_csrf_cookie
from django.http.response import HttpResponse, HttpResponseForbidden
import json
from map.models import ObjectType, UserArea
from ppamap.security_utils import check_permissions


def show_tutorial(request):
  if request.user.is_authenticated():
    return render(request , 'tutorial.html')
  return HttpResponseForbidden() 


def create_view(request):  
  return render(request, "base.html", {
              "events": _get_event_list(),
              "factions": _get_fraction_list(),
              "obj_type": _get_object_list(),
              })


def get_data(request):
  return HttpResponse(json.dumps({"pois": poi._get_poi_data(POI.objects.all()),
                                  "polys": poly._get_poly_data(Polygons.objects.all())}),
                      content_type='application/json')


def _get_event_list():
  return list(EventType.objects.all())



def _get_object_list():
  return list(ObjectType.objects.all())



def _get_fraction_list():
  return list(Faction.objects.all())


@ensure_csrf_cookie
def get_filtered_data(request):
  post = request.POST
  try:
    events = post.getlist('events[]')
    factions = post.getlist('factions[]')
    objects = post.getlist('objects[]')
    if not events and not factions and not objects:
      polys = Polygons.objects.all()
      pois = POI.objects.all()
    else:
      polys = poly.filter_poly(factions, objects, events)
      pois = poi.filter_poi(factions, objects, events)

    polys = set(polys)
    pois = set(pois)
    return HttpResponse(json.dumps({
      "poly": poly._get_poly_data(polys),
      "poi": poi._get_poi_data(pois)
      }
     ), content_type="application/json")
  except Exception as e:
    return HttpResponse(json.dumps({'error': e.message}), content_type='application/json')


@ensure_csrf_cookie
def get_filtered_objs(request):
  post = request.POST
  try:
    pois = POI.objects.filter(action_date__range=[post['from'], post['to']])
    polys = Polygons.objects.filter(action_date__range=[post['from'], post['to']])
    return HttpResponse(json.dumps({
                                "poi": poi._get_poi_data(pois),
                                "poly": poly._get_poly_data(polys)}
                               ), content_type="application/json")
  except Exception as e:
    return HttpResponse('failed')
  
  
def get_user_area(request):
  if check_permissions(request, "map.add_poi", "map.add_polygons"):
    return HttpResponse(json.dumps({"error": "No access to data"}), content_type='application/json')
  
  user_areas = list(UserArea.objects.filter(user=request.user))
  out_user_areas = ["60,0;45,0;45,40;60,40;"]
  for area in user_areas:
    out_user_areas.append(area.area)
  return HttpResponse(json.dumps({"area":out_user_areas}), content_type='application/json')