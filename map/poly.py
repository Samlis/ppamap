from django.http.response import HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie
from django.shortcuts import render
import description
import json
from map.collisions import in_sector
from models import Polygons, EventType, Faction, ObjectType, UserArea, Description
from ppamap.security_utils import check_permissions
from ppalogger.logger import add_log

def _get_poly_data(polygons):
  poly_out = []
  for poly in list(polygons):
    poly_out.append({
                     "id": poly.id,
                     "strokecolor": poly.stroke_color,
                     "strokeopt": str(poly.stroke_opacity),
                     "strokeweight": str(poly.stroke_weight),
                     "poly_desc": _get_poly_descs(poly),
                     "fillcolor": poly.fill_color,
                     "fillopt": str(poly.fill_opacity),
                     "paths": poly.paths,
                     "date": str(poly.action_date.year) if poly.action_date else "2077", #change this to dynamic in future
                     })
  return poly_out

def _get_poly_descs(poly):
  descs = ""
  if poly.extra_desc:
    descs += poly.extra_desc
    descs += ";"
  if hasattr(poly.poly_desc, "id"):
    descs += ("%s,%s" % (poly.poly_desc.id, poly.poly_desc.title) )
  return descs.rstrip(';')

def _get_poly_for_event(events = None):
  if events:
    poly = []
    for event in events:
      event_obj = EventType.objects.get(name = event)
      poly += list(Polygons.objects.filter(event_type = event_obj))        
    return poly
  return list(Polygons.objects.all())

def filter_poly(factions = None, objects = None, events = None):
  filters = {}
  if events:
    filters['event_type__name__in'] = events
  if factions:
    filters['faction__name__in'] = factions
  if objects:
    filters['object_type__name__in'] = objects
  return Polygons.objects.filter(**filters)        


def _get_poly_for_factions(factions = None):
  if factions:
    poly = []
    for faction in factions:
      faction_obj = Faction.objects.get(name = faction)
      poly += list(Polygons.objects.filter(faction = faction_obj))        
    return poly
  return list(Polygons.objects.all())


def _get_poly_for_objects(objects = None):
  if objects:
    pois = []
    for obj in objects:
        obj_type = ObjectType.objects.get(name = obj)
        pois += list(Polygons.objects.filter(object_type = obj_type))        
    return pois
  return list(Polygons.objects.all())


def _coords_iter(coords):
  coords.pop()
  for coord in coords:
    yield coord.split(',')


@ensure_csrf_cookie
def save_polygon(request):
  post = request.POST
  if(check_permissions(request, "map.add_polygons", "map.change_polygons")):
    return HttpResponse("failed")
  try:
    faction = post.get('faction')
    obj_type = post.get('object_type')
    event_type = post.get('event_type')
    obj = None
    user_area = list(UserArea.objects.filter(user = request.user))
    in_area = False
    if not len(user_area):
      in_area = True
      
    if not post.get('poly') and post.get('id'):
      obj = Polygons.objects.get(id = post.get('id'))
      coords = obj.paths.rstrip(';').split(';')
    else:
      coords = post['poly'].rstrip(';').split(';')
    for area in user_area:
      poly_in_area = 0 
      if in_area:
        break
      for lat, lng in _coords_iter(coords):
        if in_sector(float(lat), float(lng), area.area):
          poly_in_area += 1
          
      if(len(coords) == poly_in_area):
        in_area = True
        break
      
    if not in_area:
      return HttpResponse(json.dumps({"error":"Polygon not in your area..."}), content_type = "application/json")
    
    if post.get('id') or obj:
      if not obj:
        obj = Polygons.objects.get(id = post.get('id'))
      if post.get('poly'):
        obj.paths = post['poly']
      if post.get('strokeColor'):
        obj.stroke_color = post['strokeColor']
      if post.get('strokeOpacity'):
        obj.stroke_opacity = post['strokeOpacity']
      if post.get('strokeWeight'):
        obj.stroke_weight = post['strokeWeight']
      if post.get('fillColor'):
        obj.fill_color = post['fillColor']
      if post.get('fillOpacity'):
        obj.fill_opacity = post['fillOpacity']
      if post.get('desc'):
        obj.extra_desc = post['desc'] if post.get('desc') != "null" else ""
      if post.get('actionDate'):
        obj.action_date = post['actionDate']
      if faction:
        obj.faction = Faction.objects.get(id = post['faction']) if faction != 'null' else None
      if obj_type:
        obj.object_type = ObjectType.objects.get(id = post['object_type']) if obj_type != 'null' else None
      if event_type:
        obj.event_type = EventType.objects.get(id = post['event_type']) if event_type != 'null' else None
      obj.user = request.user
      obj.save()
      add_log("Edited polygon #%s" % obj.id, request.user) 
    else:
      obj = Polygons.objects.create(
                            stroke_color = post.get('strokeColor'),
                            stroke_opacity = post.get('strokeOpacity'),
                            stroke_weight = post.get('strokeWeight'),
                            fill_color = post.get('fillColor'),
                            fill_opacity = post.get('fillOpacity'),
                            paths = post.get('poly'),
                            action_date = post.get('actionDate'),
                            user = request.user,
                            )
      if faction and faction != 'null':
        obj.faction = Faction.objects.get(id = post['faction'])
      if obj_type and obj_type != 'null':
        obj.object_type = ObjectType.objects.get(id = post['object_type'])
      if event_type and event_type != 'null':
        obj.event_type = EventType.objects.get(id = post['event_type'])
      obj.save()
      add_log("Created new polygon #%s" % obj.id, request.user) 
  except Exception as e:
    return HttpResponse(json.dumps({"error":e.message}), content_type='application/json')
  return HttpResponse(obj.id)


@ensure_csrf_cookie
def remove_poly(request):
  post = request.POST
  if(check_permissions(request, "map.delete_polygons")):
    return HttpResponse("0xa0")
  
  poly = Polygons.objects.get(id = post['id'])
  user_area = list(UserArea.objects.filter(user = request.user))
  in_area = False
  if not len(user_area):
    in_area = True
    
  

  for area in user_area:
    poly_in_area = 0  
    if in_area:
      break
    for lat, lng in _coords_iter(poly.paths.split(';')):
      if in_sector(float(lat), float(lng), area.area):
        poly_in_area += 1
    if(len(poly.paths.rstrip(';').split(';')) == poly_in_area):
      in_area = True
      break
  
  
  if not in_area:
    return HttpResponse(json.dumps({"error":"Polygon not in your area..."}), content_type = "application/json")
  
  try:
    poly.delete()
    add_log("Removed polygon #%s" % post['id'], request.user)
  except:
    return HttpResponse('0')
  return HttpResponse('1')


@ensure_csrf_cookie
def get_poly_property_dialog(request):
  post = request.POST
  poly = None
  
  if(check_permissions(request, "map.add_polygons", "map.change_polygons")):
    return HttpResponse(json.dumps({"error":"No access to adding/editing poi"}), content_type="application/json")
   
  if post.get('id'):
    poly = Polygons.objects.get(id = post.get('id'))  
    
  output_data = {
                 "poly_property": None,
                 "obj_types": list(ObjectType.objects.all()),
                 "evt_types": list(EventType.objects.all()),
                 "factions": list(Faction.objects.all()),
                }
  
  if poly:
    output_data['poly_property'] = {
                                   "id": poly.id,
                                   "obj_type_id": poly.object_type.id if poly.object_type else None,
                                   "evt_type_id": poly.event_type.id if poly.event_type else None,
                                   "fct_id": poly.faction.id if poly.faction else None,
                                   "stroke_weight": poly.stroke_weight,
                                   "stroke_color": poly.stroke_color,
                                   "stroke_opacity": poly.stroke_opacity,
                                   "fill_color": poly.fill_color,
                                   "fill_opacity": poly.fill_opacity,
                                   "action_date": poly.action_date.strftime('%Y-%m-%d') if poly.action_date else "",
                                   }  
    output_data['descriptions'], output_data['used_descriptions'] = _sort_used_and_unused_descriptions(poly)
  else:
    output_data['descriptions'] = list(Description.objects.all())
   
  return render(request, "poly-property-dialog.html", output_data)
  
  
  
def _sort_used_and_unused_descriptions(poly):
    descriptions = list(Description.objects.all())
    poly_descs = []
    used_descs = []
    if poly.poly_desc:
      poly_descs.append(poly.poly_desc.id)
    if poly.extra_desc:
      poly_descs += [x.split(',')[0] for x in poly.extra_desc.split(';')]
    
    for desc in list(descriptions):
      if str(desc.id) in poly_descs:
        used_descs.append(desc)
        descriptions.remove(desc)
    
    return (descriptions, used_descs)